﻿using UnityEngine;
using System.Collections;

public class LastHit : MonoBehaviour
{
    public int id;
    public float elapsedTime;
    private bool start;
    // Use this for initialization
    void Start()
    {
        id = -1;
        elapsedTime = 0.0f;
        start = false;
    }

    void OnCollisionEnter(Collision col)
    {
        if (col.gameObject.tag == "Player")
        {
            start = true;
            elapsedTime = 0.0f;
            id = col.gameObject.GetComponent<PhotonView>().ownerId;
        }
    }

    // Update is called once per frame
    void Update()
    {
        if(start)
        {
            elapsedTime += Time.deltaTime;
            if(elapsedTime > 3.0f)
            {
                elapsedTime = 0.0f;
                start = false;
            }
        }
    }
}
