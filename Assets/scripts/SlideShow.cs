﻿using UnityEngine;
using UnityEngine.UI;
using System.Collections;

public class SlideShow : MonoBehaviour
{

    public Sprite[] sprites;
    private Sprite curSprite;
    public Image bgImage;
    private float posX, posY, posZ;
    public float timeLimit = 4f;
    private float elapsedTime = 0.0f;
    private int curIndex = 0;
    
    // Use this for initialization
    void Start()
    {
        posX = bgImage.GetComponent<RectTransform>().position.x;
        posY = bgImage.GetComponent<RectTransform>().position.y;
        posZ = bgImage.GetComponent<RectTransform>().position.z;
        if (sprites.Length > 0) curSprite = sprites[curIndex++];
    }

    // Update is called once per frame
    void Update()
    {
        elapsedTime += Time.deltaTime;
        if(bgImage.GetComponent<Image>().sprite != curSprite)
        {
            bgImage.GetComponent<Image>().sprite = curSprite;
            
        }
        else if(elapsedTime < 4.0f)
        {
            Vector3 newPos = new Vector3(bgImage.GetComponent<RectTransform>().position.x - Time.deltaTime * 10f, posY, posZ);
            bgImage.GetComponent<RectTransform>().position = newPos;
        }
        else if(elapsedTime < 8.0f)
        {
            Vector3 newPos = new Vector3(bgImage.GetComponent<RectTransform>().position.x + Time.deltaTime * 10f, posY, posZ);
            bgImage.GetComponent<RectTransform>().position = newPos;
        }

        if (elapsedTime > 8.0f)
        {
            elapsedTime = 0.0f;
            Vector3 newPos = new Vector3(posX, posY, posZ);
            bgImage.GetComponent<RectTransform>().position = newPos;
            curSprite = sprites[curIndex % sprites.Length];
            curIndex++;
        }

        if(Input.GetKeyDown(KeyCode.Escape))
        {
            PhotonNetwork.Disconnect();
            Application.LoadLevel("splash");
        }
        
    }
}
