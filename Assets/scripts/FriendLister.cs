﻿using UnityEngine;
using UnityEngine.UI;
using System.Collections;
using System.Collections.Generic;

public class FriendLister : MonoBehaviour
{
    public GameObject button;
    GameObject j;
    GameObject parent;
    string name;
    string room;
    bool online;
    bool inRoom;
    friends_list f;
    List<FriendInfo> friendList;
    stack s;
    
    // Use this for initialization

    void Start()
    {
        s = new stack();
        j = GameObject.Find("Join");
        parent = GameObject.Find("Panel");
        f = new friends_list();
        f.add_friend("nahid");
        f.friends_now_update();
        ListFriends();
    }

    // Update is called once per frame
    void Update()
    {
        if (Input.GetKeyDown(KeyCode.Escape))
        {
            s.pop();
            Application.LoadLevel(s.top());

        }
    }

    public void ListFriends()
    {
        

        friendList = f.get_updated_friends();
        Debug.Log(friendList[0]);
        foreach(FriendInfo i in friendList)
        {
            GameObject obj = (GameObject)Instantiate(button);
                obj.transform.parent = parent.transform;
                obj.GetComponentInChildren<Text>().text = i.Name;
                if (i.IsOnline)
                {
                    obj.GetComponentInChildren<Text>().text += " is online";

                    if (i.IsInRoom)
                    {
                        obj.GetComponentInChildren<Text>().text += " in " + i.Room;
                    }
                }

                else
                    obj.GetComponentInChildren<Text>().text += " is offline";
        }
    }
}
