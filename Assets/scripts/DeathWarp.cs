﻿using UnityEngine;
using System.Collections;

public class DeathWarp : MonoBehaviour
{
    public AudioClip whoosh;
    public GameObject pl;
    public bool animStart;
    public bool animEnd;
    public float animTime;
    public bool once;
    public int id;
    // Use this for initialization
    void Start()
    {
        once = false;
        animStart = true;
        animEnd = false;
        animTime = 0.0f;

        
    }

    // Update is called once per frame
    void Update()
    {
        if (pl == null)
        {
            try
            {
                foreach (GameObject o in GameObject.FindGameObjectsWithTag("Player"))
                {
                    if (o.GetComponent<PhotonView>().ownerId == id)
                        pl = o;
                }
            }
            catch
            {

            }
        }
        else
        {

            if (animStart && !animEnd)
            {
                if (!once)
                {
                    audio.PlayOneShot(whoosh);
                    once = true;
                }
                Debug.Log("ANIM");
                pl.GetComponent<MeshRenderer>().enabled = false;
                if (animTime < 2.0f)
                {
                    gameObject.GetComponentInChildren<ParticleEmitter>().minSize -= (Time.deltaTime * 10);
                    gameObject.GetComponentInChildren<ParticleEmitter>().maxSize -= (Time.deltaTime * 10);
                    animTime += Time.deltaTime;
                }
                else
                {
                    animEnd = true;
                }
            }
            else if (animStart)
            {

                
                Vector3 pos = GameObject.Find("Death").transform.position;
                Vector3 point = new Vector3(pos.x, pos.y + 4, pos.z);

                pl.rigidbody.velocity = Vector3.zero;
                pl.transform.rotation = Quaternion.identity;
				if(pl.GetComponent<PhotonView>().isMine)
                	GameObject.Find("Global").GetComponent<CameraManager>().enabled = false;
                pl.transform.position = point;
                pl.GetComponent<MeshRenderer>().enabled = true;
                GameObject.Find("arena").GetComponent<XP>().xpEnabled = false;
                pl.GetComponent<Death>().death = false;
                pl.GetComponent<Death>().once = false;
                
                Destroy(this.gameObject);
            }
        }
    }
}
