﻿using UnityEngine;
using System.Collections;

public class ReduceVelocity : MonoBehaviour
{

    // Use this for initialization
    void Start()
    {

    }

    void OnTriggerEnter(Collider col)
    {
        Debug.Log("Reducing");
        Rigidbody r = col.gameObject.rigidbody;
        r.velocity = new Vector3(r.velocity.x, r.velocity.y * 0.5f, r.velocity.z);
    }

    // Update is called once per frame
    void Update()
    {

    }
}
