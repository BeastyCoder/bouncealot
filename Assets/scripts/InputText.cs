﻿using UnityEngine;
using UnityEngine.UI;
using System.Collections;

public class InputText : MonoBehaviour
{
    private string roomName;
    public InputField inField;
    public Text numPlayer;
    public GameObject alertBox;
    string x;
    int i;
    stack s;
    // Use this for initialization
    
    void Start()
    {
        s = new stack();
        i = 1;
        numPlayer.text = i.ToString();
        x = "avatar_select";
    }

    public void OnEnteringName(int type)
    {
        
        roomName = inField.text;
        
        //Debug.Log(roomName);
        if (type == 0)
        {
            if (roomName.Length > 0)
            {
                GameObject.Find("Join").GetComponent<join>().minimum_players = i;
                Debug.Log(i);
                if (GameObject.Find("Join").GetComponent<join>().SetRoomName(roomName, 0))
                {
                    Invoke("Change", 0.7f);
                }
                else
                {                   
                    alertBox.gameObject.SetActive(true);
                    alertBox.gameObject.GetComponentInChildren<Text>().text = "Oops room already taken. Try another name.";
                }
            }
            else
            {               
                alertBox.gameObject.SetActive(true);
                alertBox.gameObject.GetComponentInChildren<Text>().text = "Forgot something ?";
            }
        }
        else if(type == 2)
        {
            if (roomName.Length > 0)
            {
                GameObject.Find("Join").GetComponent<join>().minimum_players = i;
                if (GameObject.Find("Join").GetComponent<join>().SetRoomName(roomName, 2))
                {
                    Invoke("Change", 0.7f);
                }
                else
                {
                    alertBox.gameObject.SetActive(true);
                    alertBox.gameObject.GetComponentInChildren<Text>().text = "Oops room already taken. Try another name.";
                }
            }
            else
            {
                alertBox.gameObject.SetActive(true);
                alertBox.gameObject.GetComponentInChildren<Text>().text = "Forgot something ?";
            }
        }
        else if (type == 3)
        {
            if (roomName.Length > 0)
            {
                
                Debug.Log(roomName);
                if (!GameObject.Find("Join").GetComponent<join>().SetRoomName(roomName, 3))
                {
                    Invoke("Change", 0.7f);
                }
                else
                {
                    alertBox.gameObject.SetActive(true);
                    alertBox.gameObject.GetComponentInChildren<Text>().text = "Oops no such room found. Do you have the right name ?";
                }
            }
            else
            {
                alertBox.gameObject.SetActive(true);
                alertBox.gameObject.GetComponentInChildren<Text>().text = "Forgot something ?";
            }
        }
    }

    void Change()
    {
        s.push(x);
        Application.LoadLevel(x);
    }

    public void Increment()
    {
        if (i < 4)
        {
            i++;
            numPlayer.text = i.ToString();
        }
    }

    public void Decrement()
    {
        if(i > 1)
        {
            i--;
            numPlayer.text = i.ToString();
        }
    }

    public void UpdatePrefs()
    {

        GameObject.Find("Join").GetComponent<PlayerName>().ChangePlayerName(inField.text);
        Application.LoadLevel("main");
    }

    // Update is called once per frame
    void Update()
    {
        if (Input.GetKeyDown(KeyCode.Escape))
        {
            if (s.top() == "main")
            {
                Application.Quit();
            }
            else
            {
                s.pop();
                Application.LoadLevel(s.top());
            }
        }

        
    }
}
