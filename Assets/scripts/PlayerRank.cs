﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;

public class PlayerRank: MonoBehaviour
{
    public List<PlayerInfo> l;
    void Start()
    {
        l = new List<PlayerInfo>();
    }

    /*void OnGUI()
    {
        GUILayout.BeginArea(new Rect(0, Screen.height / 2.0f + 50f, 200, 200));
        GUILayout.BeginVertical();
        foreach(PlayerInfo i in l)
        {
            GUILayout.BeginHorizontal();
            GUILayout.Label(i.PlayerName);
            GUILayout.Label(i.PlayerCoin.ToString());
            GUILayout.EndHorizontal();
        }
        GUILayout.EndVertical();
        GUILayout.EndArea();
    }*/

    void Update()
    {
        foreach(GameObject pl in GameObject.FindGameObjectsWithTag("Player"))
        {
            PlayerInfo o = l.Find(x => x.PlayerId == pl.GetComponent<PhotonView>().ownerId);
            if(o != null)
            {
                o.PlayerCoin = pl.GetComponent<PlayerCoin>().totalCoin;
            }
            else
            {
                PlayerInfo i = new PlayerInfo();
                i.PlayerCoin = pl.GetComponent<PlayerCoin>().totalCoin;
                i.PlayerId = pl.GetComponent<PhotonView>().ownerId;
                i.PlayerName = pl.GetComponent<PhotonView>().owner.name;
                //Debug.Log(pl.GetComponent<PhotonView>().isMine);
                i.IsPlayer = pl.GetComponent<PhotonView>().isMine;
                l.Add(i);
            }
        }
        l.Sort(delegate(PlayerInfo p1, PlayerInfo p2) { return p2.PlayerCoin.CompareTo(p1.PlayerCoin); });
        if(l.Count > 0)
            if (l[0].PlayerCoin >= 100) Application.LoadLevel("endresults");
    }
}
