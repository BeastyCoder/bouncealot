﻿using UnityEngine;
using UnityEngine.UI;
using System.Collections;

public class AddFriendC : MonoBehaviour
{
    stack s;
    public InputField intex;
    public GameObject alertBox;
    public void Add()
    {
        if(intex.text != "")
        {
            if (intex.text != GameObject.Find("Join").GetComponent<join>().authName)
            {
                GetComponent<add_friend>().post_friend(GameObject.Find("Join").GetComponent<join>().authName, intex.text);
            }
            else
            {
                alertBox.gameObject.SetActive(true);
                alertBox.gameObject.GetComponentInChildren<Text>().color = new Color32(105, 5, 5, 255);
                alertBox.gameObject.GetComponentInChildren<Text>().text = "Are you trying to befriend yourself ?";
            }
            intex.text = "";
        }
    }

    // Use this for initialization
    void Start()
    {
        s = new stack();
    }

    // Update is called once per frame
    void Update()
    {
        if(Input.GetKeyDown(KeyCode.Escape))
        {
            
            Application.LoadLevel("main");
        }
    }
}
