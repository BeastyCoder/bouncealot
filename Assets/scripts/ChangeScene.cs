﻿using UnityEngine;
using System.Collections;

public class ChangeScene : MonoBehaviour
{
    stack s;
    string nam;
    // Use this for initialization
    void Start()
    {
        s = new stack();
    }

    // Update is called once per frame
    void Update()
    {
        if(Input.GetKeyDown(KeyCode.Escape))
        {
            Debug.Log("Escape");
            if(s.top() == "main")
            {
                Debug.Log("Here");
                Application.Quit();
            }
            else
            {
                Debug.Log("here");
                Debug.Log(s.top());
                s.pop();
                Application.LoadLevel(s.top());
            }
        }
    }

    public void LoadScene(string name)
    {
        s.push(name);
        nam = name;
        Invoke("Change", 0.7f);
    }

    void Change()
    {
        Application.LoadLevel(nam);
    }

}
