﻿using UnityEngine;
using System.Collections;

public class FriendData
{
    public GameObject button { get; set; }
    public string friendName { get; set; }
    public bool friendOnline { get; set; }
    public string friendRoom { get; set; }
    public bool friendInRoom { get; set; }

}
