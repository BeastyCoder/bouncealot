﻿using UnityEngine;
using System.Collections;

public class Frozen : MonoBehaviour
{
    public bool frozen;
    
    bool once;
    public Texture iced;
    private Texture prevTex;

    [RPC]
    void StatusUpdate(bool stat)
    {
        frozen = stat;
    }

    // Use this for initialization
    void Start()
    {
        frozen = false;
        
        once = false;
    }

    // Update is called once per frame
    void Update()
    {
        if(frozen && !once)
        {
            Debug.Log("Frozen here");
            prevTex = GetComponent<Renderer>().material.mainTexture;
            GetComponent<Renderer>().material.mainTexture = iced;
            once = true;
            GetComponent<PhotonView>().RPC("StatusUpdate", PhotonTargets.Others, new object[] { true });
        }
        else if(!frozen && once)
        {
            Debug.Log("Unfreeze");
            GetComponent<PhotonView>().RPC("StatusUpdate", PhotonTargets.Others, new object[] { false });
            GetComponent<Renderer>().material.mainTexture = prevTex;
            once = false;
        }
    }
}
