﻿using UnityEngine;
using System.Collections;

public class XPToogleArena : MonoBehaviour {

	// Use this for initialization
	void Start () {
	
	}
	
    void OnTriggerEnter(Collider col)
    {
        if(col.gameObject.tag == "Player")
        {
            col.gameObject.GetComponent<PlayerXP>().toggle(true);
            GameObject.FindGameObjectWithTag("MainCamera").GetComponent<CameraBehave>().awayDistance = 5.0f;
        }
        
    }

    

	// Update is called once per frame
	void Update () {
	
	}
}
