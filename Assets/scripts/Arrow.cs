﻿using UnityEngine;
using System.Collections;

public class Arrow : MonoBehaviour
{
    GameObject player;
    public Texture red;
    public Texture green;
    public bool maze;
    // Use this for initialization
    void Start()
    {
        
        maze = false;
    }

    

    // Update is called once per frame
    void Update()
    {
        if (player == null)
        {
            try
            {
                foreach (GameObject pl in GameObject.FindGameObjectsWithTag("Player"))
                {
                    if (pl.GetComponent<PhotonView>().isMine)
                    {
                        player = pl;
                    }
                }
            }
            catch
            {

            }
        }
        else
        {
            transform.position = new Vector3(player.transform.position.x, player.transform.position.y + 3, player.transform.position.z);
            if (Vector3.Distance(transform.position, GameObject.FindGameObjectWithTag("beacon").transform.position) < 30.0f)
            {
                foreach (Transform g in transform)
                {
                    g.gameObject.GetComponentInChildren<Renderer>().material.mainTexture = green;
                }
            }
            else
            {
                foreach (Transform g in transform)
                {
                    g.gameObject.GetComponentInChildren<Renderer>().material.mainTexture = red;
                }
            }
            transform.LookAt(GameObject.FindGameObjectWithTag("beacon").transform.position);

            
            foreach(Transform t in transform)
            {
                  t.GetComponent<MeshRenderer>().enabled = maze;
            }
            
        }
    }
}
