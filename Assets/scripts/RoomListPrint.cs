﻿using UnityEngine;
using UnityEngine.UI;
using System.Collections;

public class RoomListPrint : MonoBehaviour
{
    public GameObject gb;
    private GameObject joinObject;
    GameObject obj;
    GameObject parent;
    stack s = new stack();
    // Use this for initialization
    void Start()
    {
        GameObject.Find("Join").GetComponent<join>().SetType(3);
        parent = GameObject.Find("Panel");
        joinObject = GameObject.Find("Join");
    }

    public void AddRoom(string name)
    {
        Debug.Log(name);
        obj = (GameObject)GameObject.Instantiate(gb);
        obj.transform.parent = parent.transform;
        obj.transform.localScale = new Vector3(1, 1, 1);
        obj.GetComponentInChildren<Text>().text = name;
        obj.GetComponent<Button>().onClick.AddListener(delegate { GameObject.Find("RoomLister").GetComponent<RoomListPrint>().JoinRoom(name); });
    }

    public void JoinRoom(string x)
    {
        Debug.Log(x);
        if(!joinObject.GetComponent<join>().SetRoomName(x,1))
        {
            Application.LoadLevel("avatar_select");
        }
        
    }

    // Update is called once per frame
    void Update()
    {
        if(Input.GetKeyDown(KeyCode.Escape))
        {
            s.pop();
            Application.LoadLevel(s.top());
        }
    }
}
