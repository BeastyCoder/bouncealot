﻿using UnityEngine;
using System.Collections;

public class test1 : MonoBehaviour {

	// Use this for initialization
	int flag=1;
	void Start () {
		PhotonNetwork.ConnectUsingSettings("0.1");
	}

	void OnJoinedLobby()
	{
		PhotonNetwork.JoinRandomRoom();
	}

	void OnPhotonRandomJoinFailed()
	{
		Debug.Log("Can't join random room!");
		PhotonNetwork.CreateRoom(null);
		flag = 2;
	}


	void OnJoinedRoom()
	{
		GameObject monster = PhotonNetwork.Instantiate("monsterprefab", Vector3.zero, Quaternion.identity, 0);
	
		CharacterControl controller = monster.GetComponent<CharacterControl>();
		controller.enabled = true;
		CharacterCamera camera = monster.GetComponent<CharacterCamera>();
		camera.enabled = true; // */
	}

	// OnGui is called once per frame
	void OnGUI()
	{
		GUILayout.Label(PhotonNetwork.connectionStateDetailed.ToString());
		GUILayout.Label("   "+flag);
	}

}
