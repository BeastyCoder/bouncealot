﻿using UnityEngine;
using System.Collections;

public class Expulso : MonoBehaviour
{
    public bool firstAbility = false;
    public float firstElapsedTime = 0.0f;
    public float firstCD = 35f;
    private bool onCD;
    public int lastColliderId = -1;
    public Vector3 lastDir = Vector3.zero;
    public float colliderElapsed = 0.0f;

    [RPC]
    void ApplyForce(Vector3 direction, int ID)
    {
        if(PhotonNetwork.player.ID == ID)
        {
            rigidbody.AddForce(direction * 100f, ForceMode.Impulse);
        }
    }

    void OnCollisionEnter(Collision col)
    {
        if (col.gameObject.tag == "Player" && GetComponent<PhotonView>().isMine)
        {
            lastColliderId = col.gameObject.GetComponent<PhotonView>().ownerId;
            lastDir = (col.gameObject.transform.position - transform.position).normalized;
            colliderElapsed = 0.0f;
        }
    }

    // Use this for initialization
    void Start()
    {
        firstElapsedTime = firstCD;
    }

    // Update is called once per frame
    void Update()
    {
        colliderElapsed += Time.deltaTime;
        if(firstAbility && !onCD && lastColliderId != -1)
        {
            firstElapsedTime = 0.0f;
            GetComponent<RPCTest>().AddForce(lastDir, 2f, 1f, lastColliderId);
            lastDir = Vector3.zero;
            lastColliderId = -1;
            onCD = true;
        }

        else if(firstAbility)
        {
            firstAbility = false;
        }

        if(onCD)
        {
            firstElapsedTime += Time.deltaTime;
            if(firstElapsedTime > firstCD)
            {
                firstElapsedTime = firstCD;
                onCD = false;
                firstAbility = false;
            }
        }

        if(colliderElapsed > 3.0f)
        {
            colliderElapsed = 0.0f;
            lastColliderId = -1;
        }
    }
}
