﻿using UnityEngine;
using UnityEngine.UI;
using System.Collections;

public class splashAnim : MonoBehaviour
{
    public Sprite[] sprites;
    float elapsedTime = 0.0f;
    float startTime = 0.0f;
    float endTime = 20.0f;
   
    public Camera cam;
    //public GameObject arena;
    int i = 0;
    public Image img;
    // Use this for initialization
    void Start()
    {
        Invoke("Change", 5);
    }

    void Change()
    {

        
        cam.gameObject.SetActive(true);
        Application.LoadLevel("login");
    }

    // Update is called once per frame
    void Update()
    {
        startTime += Time.deltaTime;
        elapsedTime += Time.deltaTime;
        if(elapsedTime > 0.1f && i < sprites.Length)
        {
            img.sprite = sprites[i++];
            elapsedTime = 0.0f;
        }

        
    }
}
