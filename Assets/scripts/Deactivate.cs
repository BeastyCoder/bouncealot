﻿using UnityEngine;
using System.Collections;

public class Deactivate : MonoBehaviour
{
    public void deactivate()
    {
        Invoke("active", 0.7f);
    }

    void active()
    {
        this.gameObject.SetActive(false);
    }
}
