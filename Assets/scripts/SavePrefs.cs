﻿using UnityEngine;
using UnityEngine.UI;
using System.Collections;

public class SavePrefs : MonoBehaviour
{
    private Slider senseSlide;
    private Slider volSlide;
    private Toggle joyTog;

    void Awake()
    {
        senseSlide = GameObject.Find("Sensitivity").GetComponent<Slider>();
        volSlide = GameObject.Find("Volume").GetComponent<Slider>();
        joyTog = GameObject.Find("Toggle").GetComponent<Toggle>();

        senseSlide.value = GetFloat("sensitivity");
        volSlide.value = GetFloat("volume");
        joyTog.isOn = GetBool("joystick");
        
    }

    float GetFloat(string key)
    {
        if (PlayerPrefs.HasKey(key))
            return PlayerPrefs.GetFloat(key);
        else return 1f;
    }

    bool GetBool(string key)
    {
        if (PlayerPrefs.HasKey(key))
            return PlayerPrefs.GetInt(key) == 1 ? true : false;
        else return false;
    }

    public void Save()
    {
        PlayerPrefs.SetFloat("sensitivity", senseSlide.value);
        PlayerPrefs.SetFloat("volume", volSlide.value);
        PlayerPrefs.SetInt("joystick", joyTog.isOn ? 1 : 0);
        Application.LoadLevel("main");
    }

    // Use this for initialization
    void Start()
    {

    }

    // Update is called once per frame
    void Update()
    {
        
    }
}
