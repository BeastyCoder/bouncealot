﻿using UnityEngine;
using System.Collections;

public class Props : MonoBehaviour
{
    public float startingSpeed = 10;
    public float curSpeed = 10;
    public float maxSpeed = 15;
    public float bumpStartScale = 0.1f;
    public float curBumpScale;
    public float bumpFinalScale = 0.2f;
    // Use this for initialization
    void Start()
    {
        GetComponent<BallMultiMove>().maxSpeed = startingSpeed;
        GetComponent<BallMultiMove>().bumpForce = 0.1f;

    }

    public void LevelUp()
    {
        curSpeed += (maxSpeed - startingSpeed) / 10;
        curBumpScale += (bumpFinalScale - bumpStartScale) / 10;
        GetComponent<BallMultiMove>().maxSpeed = curSpeed;
        GetComponent<BallMultiMove>().bumpForce = curBumpScale;
    }

    // Update is called once per frame
    void Update()
    {

    }
}
