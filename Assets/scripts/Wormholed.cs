﻿using UnityEngine;
using System.Collections;

public class Wormholed : MonoBehaviour
{
    GameObject wh;
    public bool collided;
    System.Random rand = new System.Random();
    // Use this for initialization
    void Start()
    {
        collided = false;
    }

    void OnCollisionEnter(Collision col)
    {
        if(col.gameObject.tag == "wormhole")
        {
            collided = true;
            wh = col.gameObject;
            Debug.Log("dwdwa");
        }
    }

    // Update is called once per frame
    void Update()
    {
        if(collided && !GameObject.FindGameObjectWithTag("wormhole") && !this.gameObject.GetComponent<MeshRenderer>().enabled && this.gameObject.rigidbody.constraints == RigidbodyConstraints.FreezeAll)
        {
            Debug.Log("cheese");
            GameObject[] obj = GameObject.FindGameObjectsWithTag("mazespawn");
            int i = rand.Next(0, obj.Length - 1);
            i = rand.Next(0, obj.Length - 1);
            Vector3 pos = obj[i].transform.position;
            pos.y += 1f;
            gameObject.transform.position = pos;
            rigidbody.constraints = RigidbodyConstraints.None;
            GameObject.Find("Global").GetComponent<Objective>().ChangeObjective("Follow the arrow");
            GetComponent<MeshRenderer>().enabled = true;
            GameObject.Find("Global").GetComponent<SpawnWormhole>().spawn = true;
            GameObject.Find("Global").GetComponent<SpawnWormhole>().elapsedTime = 0.0f;
            GameObject.FindGameObjectWithTag("arrow").GetComponent<Arrow>().maze = true;
            collided = false;
        }
        
    }
}
