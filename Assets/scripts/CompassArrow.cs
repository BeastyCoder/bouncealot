﻿using UnityEngine;
using System.Collections;

public class CompassArrow : MonoBehaviour
{
    GameObject coin;
    float prevAngle = 0.0f;
    float angle = 0.0f;
    GameObject player;
    // Use this for initialization
    void Start()
    {

    }

    // Update is called once per frame
    void Update()
    {
        coin = GameObject.FindGameObjectWithTag("coin");
        if (coin != null)
        {
        	if(player == null)
        	{
        		try
        		{
        			foreach(GameObject pl in GameObject.FindGameObjectsWithTag("Player"))
        			{
        				if(pl.GetComponent<PhotonView>().isMine)
        					player = pl;
        			}
        		}
        		catch
        		{}
        	}
        	else{
            Vector3 pos = new Vector3(coin.transform.position.x, 0.0f, coin.transform.position.z);
            angle = Vector3.Angle(new Vector3(1, 0, 0), pos - new Vector3(player.transform.position.x,0.0f,player.transform.position.z));
            if (pos.z < player.transform.position.z) angle = 360 - angle;
            Debug.Log(angle);
            if (prevAngle != angle)
            {
                transform.rotation = Quaternion.identity;
                transform.Rotate(new Vector3(0, 0, angle));

                prevAngle = angle;
            }
            }
        }
    }
}
