﻿using UnityEngine;
using System.Collections;

public class SpawnWormhole : MonoBehaviour
{
    System.Random rand = new System.Random();
    GameObject[] obj;
    GameObject[] arr;
    
    public float elapsedTime = 0.0f;
    public bool spawn = true;
    public int coinIndex = -1;
    public int buffIndex = -1;
    
    // Use this for initialization
    void Start()
    {
        obj = GameObject.FindGameObjectsWithTag("SpawnPoint");
        arr = GameObject.FindGameObjectsWithTag("mazespawn");
    }

    // Update is called once per frame
    void Update()
    {
        elapsedTime += Time.deltaTime;
        if (PhotonNetwork.isMasterClient)
        {
            if (elapsedTime > 20.0f && spawn && GameObject.Find("Join").GetComponent<join>().spawned)
            {
                int i = rand.Next(0, obj.Length - 1);
                
                while (i == coinIndex || i == buffIndex)
                {
                    i = rand.Next(0, obj.Length - 1);
                }
                Vector3 pos = obj[i].transform.position;
                pos.y += 1f;
                GetComponent<CoinSpawningSystem>().wormIndex = i;
                GetComponent<BuffSpawner>().wormIndex = i;
                GameObject o = PhotonNetwork.InstantiateSceneObject("wormhole", pos, Quaternion.identity, 0, null);
                o.gameObject.GetComponent<BoxCollider>().enabled = false;
                
                
                
                spawn = false;
            }
        }

    }
}
