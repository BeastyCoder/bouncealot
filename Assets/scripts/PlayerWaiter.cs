﻿using UnityEngine;
using UnityEngine.UI;
using System.Collections;

public class PlayerWaiter : MonoBehaviour
{
    public GameObject alertBox;
    public int curPlayer = 0;

    void Update()
    {
        if (PhotonNetwork.inRoom)
        {
            if (curPlayer != PhotonNetwork.room.playerCount)
            {
                curPlayer = PhotonNetwork.room.playerCount;
                alertBox.gameObject.GetComponentInChildren<Text>().text = "Waiting for " + (PhotonNetwork.room.maxPlayers - curPlayer) + " more player(s) to join.";
            }

            if (curPlayer == PhotonNetwork.room.maxPlayers)
            {
                alertBox.gameObject.SetActive(false);
            }
        }
        
    }
}
