﻿using UnityEngine;
using UnityEngine.UI;
using System.Collections;

using UnityEngine;
using System.Collections;
using System.Collections.Generic;
public class add_friend : MonoBehaviour
{
    public GameObject alertBox;
    public string unpersed;
    GameObject parent;
    public GameObject button;
    public List<FriendData> fList;
    // 
    float delay = 2f;
    float next_use;

    bool loaded = false;

    void Start()
    {
        //post_friend ("user1","analognahid");
        //get_friend ("analognahid");

        /// test login
        /*PhotonNetwork.playerName = "analognahid";
        PhotonNetwork.AuthValues = new AuthenticationValues();
        PhotonNetwork.AuthValues.AuthParameters = "username=" + "analognahid" + "&password=" + "nahid123456" + ""; //this will save the values of those variables to send via PHP
        PhotonNetwork.ConnectUsingSettings(null);*/


        parent = GameObject.Find("Panel");

        // initialize timer
        next_use = Time.time + delay;

        fList = new List<FriendData>();


    }
    void Update()
    {

        if (PhotonNetwork.Friends != null && loaded == false)
        {
            loaded = true;

            List<FriendInfo> friends = PhotonNetwork.Friends;

            foreach (FriendInfo i in friends)
            {

                if (!i.Name.Equals(""))
                {
                    FriendData d = new FriendData();
                    GameObject obj = (GameObject)Instantiate(button);
                    obj.transform.SetParent(parent.transform, false);
                    obj.transform.localScale = new Vector3(1, 1, 1);
                    obj.GetComponentInChildren<Text>().text = i.Name;
                    d.button = obj;
                    d.friendName = i.Name;
                    d.friendOnline = i.IsOnline;
                    d.friendInRoom = i.IsInRoom;
                    d.friendRoom = i.Room;
                    fList.Add(d);
                    if (i.IsOnline)
                    {
                        obj.GetComponentInChildren<Text>().text += " is online";

                        if (i.IsInRoom)
                        {
                            obj.GetComponentInChildren<Text>().text += " in " + i.Room;
                            Debug.Log(i.Room);
                            obj.GetComponent<Button>().onClick.AddListener(delegate { GameObject.Find("Script").GetComponent<JoinFriendRoom>().JoinRoom(d.friendRoom); });
                        }
                    }

                    else
                    {
                        obj.GetComponentInChildren<Text>().text += " is offline";
                    }
                    Debug.Log(i.Name + " " + i.IsOnline + " " + i.IsInRoom + " " + i.Room);
                }
            }
        }


        /// timer update
        if (Time.time > next_use)
        {//// this function is calld delay seconds later.
            next_use = Time.time + delay;
            ///do your thing

            /// check if the friend_list is ready and then update again

            Debug.Log("_" + loaded + "_");


            if (loaded == true)
            {/// the friend list is now ready to be shown ****
                List<FriendInfo> friends = PhotonNetwork.Friends;

                foreach (FriendInfo i in friends)
                {
                  
                    if (!i.Name.Equals(""))
                    {
                        FriendData dat = fList.Find(x=>x.friendName == i.Name);
                        if(dat != null)
                        {
                            
                            if(dat.friendOnline != i.IsOnline)
                            {
                                dat.friendOnline = i.IsOnline;
                                dat.friendInRoom = i.IsInRoom;
                                dat.button.GetComponentInChildren<Text>().text = i.Name;

                                if (i.IsOnline)
                                {
                                    dat.button.GetComponentInChildren<Text>().text += " is online";

                                    if (i.IsInRoom)
                                    {
                                        dat.button.GetComponentInChildren<Text>().text += " in " + i.Room;
                                        dat.button.GetComponent<Button>().onClick.AddListener(delegate { GameObject.Find("Script").GetComponent<JoinFriendRoom>().JoinRoom(i.Room); });
                                    }
                                }

                                else
                                {
                                    dat.button.GetComponentInChildren<Text>().text += " is offline";
                                    dat.button.GetComponent<Button>().onClick.RemoveAllListeners();
                                }
                            }

                            else if(dat.friendInRoom != i.IsInRoom)
                            {
                                dat.friendInRoom = i.IsInRoom;
                                dat.friendRoom = i.Room;
                                dat.button.GetComponentInChildren<Text>().text = i.Name;
                                dat.button.GetComponentInChildren<Text>().text += " is online";
                                if(i.IsInRoom)
                                { 
                                    dat.button.GetComponentInChildren<Text>().text += " in " + i.Room;
                                    Debug.Log(i.Room);
                                    dat.button.GetComponent<Button>().onClick.AddListener(delegate { GameObject.Find("Script").GetComponent<JoinFriendRoom>().JoinRoom(i.Room); });
                                }
                            }
                        }
                        else
                        {
                            FriendData d = new FriendData();
                            GameObject obj = (GameObject)Instantiate(button);
                            obj.transform.SetParent(parent.transform, false);
                            obj.transform.localScale = new Vector3(1, 1, 1);
                            obj.GetComponentInChildren<Text>().text = i.Name;
                            d.button = obj;
                            d.friendName = i.Name;
                            d.friendOnline = i.IsOnline;
                            d.friendInRoom = i.IsInRoom;
                            d.friendRoom = i.Room;
                            fList.Add(d);
                            if (i.IsOnline)
                            {
                                obj.GetComponentInChildren<Text>().text += " is online";

                                if (i.IsInRoom)
                                {
                                    obj.GetComponentInChildren<Text>().text += " in " + i.Room;
                                    Debug.Log(i.Room);
                                    obj.GetComponent<Button>().onClick.AddListener(delegate { GameObject.Find("Script").GetComponent<JoinFriendRoom>().JoinRoom(d.friendRoom); });
                                }
                            }

                            else
                            {
                                obj.GetComponentInChildren<Text>().text += " is offline";
                            }
                        }
                        
                        //Debug.Log(i.Name + " " + i.IsOnline + " " + i.IsInRoom + " " + i.Room);
                    }
                }

                PhotonNetwork.FindFriends(unpersed.Split('#'));

            }
            else
            {

                get_friend(GameObject.Find("Join").GetComponent<join>().authName); //// ;
                Debug.Log(GameObject.Find("Join").GetComponent<join>().authName);
            }

        }


    }
    public List<FriendInfo> get_updated_friends()
    {
        Debug.Log(PhotonNetwork.Friends);
        List<FriendInfo> friends = PhotonNetwork.Friends;
        Debug.Log(friends[0]);
        foreach (FriendInfo i in friends)
        {
            Debug.Log(i.Name + " " + i.IsOnline + " " + i.IsInRoom + " " + i.Room);
        }
        return friends;

    }

    public void post_friend(string id, string friend)
    {

        string url = "http://bouncealot.xerothermal.com/add.php";

        WWWForm form = new WWWForm();
        form.AddField("var1", id);
        form.AddField("var2", friend);
        WWW www = new WWW(url, form);

        StartCoroutine(WaitForRequest_post(www));
    }
    void get_friend(string id)
    {
        string url = "http://bouncealot.xerothermal.com/friends.php";

        WWWForm form = new WWWForm();
        form.AddField("var1", id);
        WWW www = new WWW(url, form);
        StartCoroutine(WaitForRequest(www));
    }

    IEnumerator WaitForRequest(WWW www)
    {
        yield return www;

        // check for errors
        if (www.error == null)
        {
            unpersed = www.text;
            unpersed = unpersed.ToLower();
            //Debug.Log(unpersed);
            //friends_arr();
            //unpersed.Split('#')
            PhotonNetwork.FindFriends(unpersed.Split('#'));

        }
        else
        {
            Debug.Log("WWW Error: " + www.error);
        }
    }
    IEnumerator WaitForRequest_post(WWW www)
    {
        yield return www;

        // check for errors
        if (www.text != "No Such Player")
        {
            Debug.Log("friend added succesfully");
            get_friend(GameObject.Find("Join").GetComponent<join>().authName);
            alertBox.gameObject.SetActive(true);
            alertBox.gameObject.GetComponentInChildren<Text>().color = new Color32(62, 105, 5, 255);
            alertBox.gameObject.GetComponentInChildren<Text>().text = "Friend added successfully.";          
        }
        else
        {
            Debug.Log("WWW Error: " + www.error);
            alertBox.gameObject.SetActive(true);
            alertBox.gameObject.GetComponentInChildren<Text>().color = new Color32(105, 5, 5, 255);
            alertBox.gameObject.GetComponentInChildren<Text>().text = "Oops no such user found";
        }
    }
    void friends_arr()
    {
        //	int position = temp.LastIndexOf ('$');
        int len = unpersed.Split('#').Length - 1;
        foreach (string s in unpersed.Split('#'))
        {
            Debug.Log(s);
        }
    }
    void update_friends()
    {
        string user = GetComponent<join>().authName;
        get_friend(user);

    }
}
