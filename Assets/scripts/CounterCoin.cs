﻿using UnityEngine;
using System.Collections;

public class CounterCoin : MonoBehaviour
{
    private int i = 1;
    public int prevId = -1;
    // Use this for initialization
    void Start()
    {

    }

    // Update is called once per frame
    void Update()
    {

    }

    public int CounterCombo(int id)
    {
        //Debug.Log("CounterCombo : " + id + "prev : " + prevId);
        if (prevId == id)
            return ++i;
        else
        {
            prevId = id;
            i = 1;
            return i;
        }
    }
}
