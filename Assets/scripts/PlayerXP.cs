﻿using UnityEngine;
using UnityEngine.UI;
using System.Collections;

public class PlayerXP : MonoBehaviour
{ 
    public int curXP;
    public int maxXP = 100;
    public int level = 1;
    GameObject bImage;
    public float elapsedTime = 0.0f;
    public bool xpEnabled = true;
    int totalXP = 0;
    GameObject txt;
    float cdElapsedTime;
    bool onCD;

    public void AddXP(int x)
    {
        Reset();
        curXP += x;
        txt.GetComponent<Text>().text = "+" + x;
        Invoke("Reset", 3);       
    }

    void Reset()
    {
        txt.GetComponent<Text>().text = "";
    }

    public void toggle(bool x)
    {
        xpEnabled = x;
    }

    public void LevelUp()
    {
        if (level < 10)
        {
            curXP = curXP % maxXP;
            maxXP = maxXP + (50 * level);
            level++;
            GetComponent<Props>().LevelUp();
        }

        if (level == 10) curXP = maxXP;
    }

    void Start()
    {
        if (GetComponent<PhotonView>().isMine)
        {
            bImage = GameObject.Find("ButtonImage");
            bImage.gameObject.SetActive(false);
        }
        txt = GameObject.Find("XPText");
        cdElapsedTime = 0.0f;
        onCD = false;
    }

    void OnCollisionEnter(Collision col)
    {
        if(col.gameObject.tag == "Player" && !onCD)
        {
            AddXP((int) Mathf.Floor(Mathf.Abs(GetComponent<Rigidbody>().velocity.magnitude))*10);
        }
    }

    void Update()
    {
        
        if (xpEnabled && GetComponent<PhotonView>().isMine)
        {
            elapsedTime += Time.deltaTime;
            if (elapsedTime > 1f)
            {
                elapsedTime = 0.0f;
                curXP++;
            }
            if (curXP > maxXP)
            {
                LevelUp();
            }
        }
        if (level > 4)
        {
            bImage.gameObject.SetActive(true);
        }

        if(onCD)
        {
            cdElapsedTime += Time.deltaTime;
            if (cdElapsedTime > 0.5f)
            {
                cdElapsedTime = 0.0f;
                onCD = false;
            }
        }
    }
}
