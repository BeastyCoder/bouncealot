﻿using UnityEngine;
using System.Collections;

public class logout : MonoBehaviour
{

    // Use this for initialization
    void Start()
    {

    }

    public void Logout()
    {
        PhotonNetwork.Disconnect();
        if (PlayerPrefs.HasKey("autoLog"))
            PlayerPrefs.SetInt("autoLog", 0);
        Application.LoadLevel("login");
    }

    // Update is called once per frame
    void Update()
    {

    }
}
