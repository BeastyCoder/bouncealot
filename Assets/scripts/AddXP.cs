﻿using UnityEngine;
using System.Collections;

public class AddXP : MonoBehaviour
{
    int i = 1;
    // Use this for initialization
    void Start()
    {
        GetComponent<PhotonView>().RPC("SetSpawn", PhotonTargets.All, new object[] { false });
    }

    [RPC]
    void SetSpawn(bool x)
    {
        GameObject.Find("Global").GetComponent<BuffSpawner>().spawn = x;
    }

    [RPC]
    void DestroyBuff()
    {

        PhotonNetwork.Destroy(GetComponent<PhotonView>());
        GameObject.Find("Global").GetComponent<BuffSpawner>().spawn = true;
        GameObject.Find("Global").GetComponent<BuffSpawner>().elapsedTime = 0.0f;
    }

    void OnTriggerEnter(Collider col)
    {
        if (col.gameObject.tag == "Player")
        {
            if (col.gameObject.GetComponent<PhotonView>().ownerId == PhotonNetwork.player.ID)
            {
                col.gameObject.GetComponent<PlayerXP>().AddXP(500);
                GetComponent<PhotonView>().RPC("SetSpawn", PhotonTargets.All, new object[] { false });
                GetComponent<PhotonView>().RPC("DestroyBuff", PhotonTargets.MasterClient, new object[] { });
                
            }

        }
    }
    // Update is called once per frame
    void Update()
    {
        
    }
}
