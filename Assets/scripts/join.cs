﻿using UnityEngine;
using System.Collections;
using System.Net;

public class join : MonoBehaviour
{

    // Use this for initialization
    int flag = 1;
    public GameObject Juggernaut, Expulso, Trickster, Frostbound, Tranquil;
    private GameObject user_prefab;
    public GameObject global;
    int joinType = -1;

    private string roomName = "";
    private const string pvt_room_key = "PRIVATE_ah1j_as%Tsd25Drfydgf_KEY";
    private string final_foom_name;
    private RoomInfo[] roomsList;
    private string[] room_names;
    private int gui_phase = 0; // gui phase 0 is selectiong room type and gui_phase 1 is selecting ball 
    private int room_type;// what type of room is gonna create. general create/join or pvt create /join

    public float delay = 30f;
    public float next_use;
    public bool ping_test = false;



    // // room wait

    public int minimum_players = -1;
    public float room_delay = 1f;
    public float room_next_use;
    public bool spawned = false;

    /// authenticate
    /// 
    public string authName = "analognahid";
    public string authToken = "nahid123456";
    private string authDebugMessage = string.Empty;




    void Start()
    {

        next_use = Time.time + delay;

        room_next_use = Time.time + room_delay; ;


        PhotonNetwork.MaxResendsBeforeDisconnect = 6;/// only tries twice to reconnect. 
        ///default 6. so less delay now
        //PhotonNetwork.ConnectUsingSettings(null);

    }



    // Update is called once per frame
    void Update()
    {

        //Debug.Log( Application.internetReachability);


        if (Time.time > next_use)
        {//// this function is calld delay seconds later.
            next_use = Time.time + delay;
            ///do your thing


            //Debug.Log(PhotonNetwork.connected + " concting" + PhotonNetwork.connecting + " " + PhotonNetwork.connectionState + " " + Time.time);
            //Debug.Log(CheckForInternetConnection()+"____is connectetion available");
            if (PhotonNetwork.connected == false)
            {

                //stack mystack = new stack();
                //mystack.start_stack();
                //Destroy(GameObject.Find("Join"));
                //Destroy(GameObject.Find("menuCam"));
                //Application.LoadLevel("splash");



            }

            

        }




        ////////////////////
        /// ///////////////
        if (Time.time > room_next_use && spawned == false)
        {//// this function is calld delay seconds later.
            room_next_use = Time.time + room_delay;
            ///do your thing


            if (PhotonNetwork.inRoom)
            {
                //Debug.Log("players number: " + PhotonNetwork.room.playerCount);
                if (PhotonNetwork.room.playerCount >= PhotonNetwork.room.maxPlayers)
                {
                    //GameObject[] spawnPoints = GameObject.FindGameObjectsWithTag("SpawnPoint");
                    //GameObject obj = PhotonNetwork.Instantiate(user_prefab.name, spawnPoints[Random.Range(1, spawnPoints.Length)].transform.position,Quaternion.identity, 0);
                    
                    Debug.Log("  GAME ON ");
                    spawned = true;

                    GameObject obj = PhotonNetwork.Instantiate(user_prefab.name, new Vector3(0, 10f, 0), Quaternion.identity, 0);
                    obj.rigidbody.constraints = RigidbodyConstraints.FreezeAll;

                    obj.transform.position = new Vector3(15f + obj.GetComponent<PhotonView>().ownerId *(-5f), 10f, 0f);
                    obj.rigidbody.constraints = RigidbodyConstraints.None;

                }
            }

        }

    }
    public void OnCustomAuthenticationFailed(string debugMessage)
    {
        GameObject.Find("Script").GetComponent<Login>().LoginFailed();
        /// here is the error message 
        Debug.Log(debugMessage);
    }


    public void connect()
    {
        PhotonNetwork.AuthValues = new AuthenticationValues();
        PhotonNetwork.AuthValues.AuthParameters = "username=" + this.authName + "&password=" + this.authToken + "";
        //this will save the values of those variables to send via PHP

        PhotonNetwork.playerName = this.authName;
        PhotonNetwork.ConnectUsingSettings(null);
    }
    void OnJoinedLobby()
    {
        //PhotonNetwork.JoinRandomRoom();
        
        //GetComponent<friends_list>().friends_now_update();
        Application.LoadLevel("main");
    }

    void OnPhotonRandomJoinFailed()
    {
        Debug.Log("Can't join random room!");
        PhotonNetwork.CreateRoom(null);
        flag = 2;
    }


    void OnJoinedRoom()
    {
        // GameObject myPlayer = PhotonNetwork.Instantiate("Player_MP", spawnPoints[Random.Range(1, spawnPoints.Length)].transform.position, spawnPoints[Random.Range(1, spawnPoints.Length)].transform.rotation, 0);
        

        //Debug.Log("joined to Room");
    }


    // OnGui is called once per frame
    void OnGUI()
    {
        GUILayout.Label(PhotonNetwork.connectionState.ToString());
        GUILayout.Label(PhotonNetwork.connectionStateDetailed.ToString());

        if (joinType == 3)
        {
            for (int i = 0; i < roomsList.Length; i++)
            {
                if (roomsList[i].playerCount == roomsList[i].maxPlayers) continue;
                if (roomsList[i].name.Contains(pvt_room_key)) continue;/// don't show the private rooms
                ///
                GameObject.Find("RoomLister").GetComponent<RoomListPrint>().AddRoom(roomsList[i].name);

            }
            joinType = -1;
        }

    }

    public void join_or_create_room()
    {
        Application.LoadLevel("integrated");
        if (room_type == 0 || room_type == 2)
        {// create general// private room
            PhotonNetwork.CreateRoom(final_foom_name, true, true, minimum_players);
        }
        else if (room_type == 1 || room_type == 3)
        {
            PhotonNetwork.JoinRoom(final_foom_name);
        }


    }

    public bool SetRoomName(string name, int type)
    {
        bool flag = false;
        OnReceivedRoomListUpdate();
        //Debug.Log(type);
        switch (type)
        {
            case 0: //Create Normal
                if (System.Array.IndexOf(room_names, name) < 0)
                {
                    final_foom_name = name;
                    room_type = 0;
                    flag = true;
                }
                break;

            case 1: //Join
                final_foom_name = name;
                room_type = 1;
                break;

            case 2: //Create Private Room
                if (System.Array.IndexOf(room_names, pvt_room_key + name) < 0)
                {
                    final_foom_name = pvt_room_key + name;
                    room_type = 2;// join private room
                    flag = true;
                }
                break;

            case 3: //Join Private room
                Debug.Log(name);
                if (System.Array.IndexOf(room_names, pvt_room_key + name) >= 0)
                {
                    final_foom_name = pvt_room_key + name;
                    room_type = 3;

                }
                else
                {
                    flag = true;
                }
                break;
        }
        return flag;
    }

    public void SetAvatar(int x)
    {
        switch (x)
        {
            case 0:
                user_prefab = Expulso;
                break;
            case 1:
                user_prefab = Frostbound;
                break;

            case 2:
                user_prefab = Juggernaut;
                break;

            case 3:
                user_prefab = Tranquil;
                break;

            case 4:
                user_prefab = Trickster;
                break;

        }
    }

    public void SetType(int type)
    {
        joinType = type;
    }


    void OnReceivedRoomListUpdate()
    {
        roomsList = PhotonNetwork.GetRoomList();
        room_names = new string[roomsList.Length];
        for (int i = 0; i < roomsList.Length; i++)
        {
            room_names[i] = roomsList[i].name;
        }

    }

}
