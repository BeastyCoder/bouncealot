﻿using UnityEngine;
using System.Collections;

public class Frostbound : MonoBehaviour
{

    public bool firstAbility = false;
    public Texture iced;
    public Texture prevTex;
    public float firstCD = 30f;
    public float firstElapsedTime;
    private bool once = false;

    [RPC]
    void Freeze(int id)
    {
        Debug.Log("called");
        foreach(GameObject pl in GameObject.FindGameObjectsWithTag("Player"))
        {
            if (pl.GetComponent<PhotonView>().isMine && pl.GetComponent<PhotonView>().ownerId == id)
            {
                pl.rigidbody.constraints = RigidbodyConstraints.FreezeAll;
                pl.GetComponent<Frozen>().frozen = true;
            }
        }
    }

    [RPC]
    void UnFreeze()
    {
        foreach(GameObject pl in GameObject.FindGameObjectsWithTag("Player"))
        {
            if (pl.GetComponent<PhotonView>().isMine)
            {
                pl.rigidbody.constraints = RigidbodyConstraints.None;
                pl.GetComponent<Frozen>().frozen = false;
            }
        }
    }

    // Use this for initialization
    void Start()
    {
        firstElapsedTime = firstCD;
    }

    // Update is called once per frame
    void Update()
    {
        if (firstAbility)
        {
            if (!once)
            {
                Collider[] colliders = Physics.OverlapSphere(transform.position, 10.0f);
                foreach (Collider col in colliders)
                {
                    if (col.gameObject.tag == "Player")
                    {
                        GetComponent<PhotonView>().RPC("Freeze", PhotonTargets.Others, new object[] {col.gameObject.GetComponent<PhotonView>().ownerId });
                        
                    }
                }
                firstElapsedTime = 0.0f;
                once = true;
            }
            if (firstElapsedTime < firstCD)
            {
                firstElapsedTime += Time.deltaTime;
                if (firstElapsedTime > 3.0f)
                    GetComponent<PhotonView>().RPC("UnFreeze", PhotonTargets.Others, new object[] { });
            }
            else
            {
                firstElapsedTime = firstCD;
                firstAbility = false;
                once = false;
            }
        }
    }
}
