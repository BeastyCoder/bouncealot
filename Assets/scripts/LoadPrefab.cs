﻿using UnityEngine;
using System.Collections;

public class LoadPrefab : MonoBehaviour
{
    public GameObject[] prefabs;
    int curIndex = 0;
    int prevIndex = -1;
    private GameObject curObj = null;
    GameObject script;
	GameObject mCam;
    GameObject mArena;
    stack s;
    // Use this for initialization
    void Start()
    {
        s = new stack();
        script = GameObject.Find("ScriptRunner");
        if (prefabs.Length > 0)
        {
            GameObject obj = (GameObject)GameObject.Instantiate(prefabs[curIndex], new Vector3(48, 1.48f, -138), Quaternion.identity);
            curObj = obj;
            prevIndex = 0;
        }
		mCam = GameObject.Find ("menuCam").gameObject;
        
		mCam.SetActive (false);
    }

    public void SetPrefabIndex()
    {
        if (prefabs.Length > 0)
            curIndex = (curIndex + 1) % prefabs.Length;

        Debug.Log(curIndex);
    }

    public void SelectAvatar()
    {
		Destroy (GameObject.Find ("menuCam"));
        
        GameObject.Find("Join").GetComponent<join>().SetAvatar(curIndex);
        GameObject.Find("Join").GetComponent<join>().join_or_create_room();

    }

    // Update is called once per frame
    void Update()
    {
        if (Input.GetKeyDown(KeyCode.Escape))
        {
            s.start_stack();
			if(mCam)
			mCam.SetActive(true);
            mArena.gameObject.SetActive(true);
            Application.LoadLevel("main");

        }
        if (prefabs.Length > 0 && curIndex != prevIndex)
        {
            GameObject.Destroy(curObj);
            GameObject obj = (GameObject)GameObject.Instantiate(prefabs[curIndex], new Vector3(48, 1.48f, -138), Quaternion.identity);
            curObj = obj;
            prevIndex = curIndex;
        }
    }
}
