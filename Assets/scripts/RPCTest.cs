﻿using UnityEngine;
using System.Collections;

public class RPCTest : MonoBehaviour
{
    Vector3 dir;
    int target;
    float mag;

    // Use this for initialization
    void Start()
    {

    }

    // Update is called once per frame
    void Update()
    {
        
    }

   
  
    [RPC]
    public void ApplyForce(Vector3 dir, float magnitude, float someFact, int targetId)
    {
        
        if (PhotonNetwork.player.ID == targetId)
        {
            Debug.Log(PhotonNetwork.player.ID + " " + GetComponent<PhotonView>().ownerId + " " + targetId + " " + dir + " " + magnitude +" " + someFact);
            foreach (GameObject obj in GameObject.FindGameObjectsWithTag("Player"))
            {
                if(obj.GetComponent<PhotonView>().ownerId == targetId)
                    obj.GetComponent<Rigidbody>().AddForce(dir.normalized * magnitude * someFact * 50f, ForceMode.Impulse);
            }
        }
    }

    public void AddForce(Vector3 direction, float magnitude , float someFact, int targetId)
    {
        if(GetComponent<PhotonView>().isMine)
        GetComponent<PhotonView>().RPC("ApplyForce", PhotonTargets.All, new object[] { direction, magnitude, someFact, targetId });
    }
}
