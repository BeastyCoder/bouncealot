﻿using UnityEngine;
using System.Collections;

public class PlayerCoin : MonoBehaviour
{
    public int totalCoin = 0;

    // Use this for initialization
    void Start()
    {

    }

    [RPC]
    void UpdateCoin(int id, int score)
    {
        if (!GetComponent<PhotonView>().isMine)
            totalCoin = score;
    }

    [RPC]
    void SetCounter(int id)
    {
        GameObject.Find("Global").GetComponent<CounterCoin>().prevId = id;
    }

    public void AddCoin(int i)
    {
        if(i > 0)
        {
            totalCoin += i;
        }
        else
        {
            if (totalCoin >= 10) totalCoin += i;
        }
        
        GetComponent<PhotonView>().RPC("SetCounter", PhotonTargets.Others, new object[] { GetComponent<PhotonView>().ownerId });
        GetComponent<PhotonView>().RPC("UpdateCoin", PhotonTargets.Others, new object[] { GetComponent<PhotonView>().ownerId, totalCoin });
    }

    // Update is called once per frame
    void Update()
    {

    }
}
