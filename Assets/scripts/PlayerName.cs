﻿using UnityEngine;
using System.Collections;

public class PlayerName : MonoBehaviour
{
    
    // Use this for initialization
    void Start()
    {
        if(!PlayerPrefs.HasKey("playerName"))
        {
            PlayerPrefs.SetString("playerName", "Default");
        }
        PhotonNetwork.playerName = PlayerPrefs.GetString("playerName");
    }

    public void ChangePlayerName(string name)
    {
        PlayerPrefs.SetString("playerName", name);
        PhotonNetwork.playerName = name;
    }

    // Update is called once per frame
    void Update()
    {

    }
}
