﻿using UnityEngine;
using UnityEngine.UI;
using System.Collections;

public class LoadingScreen : MonoBehaviour
{
    public GameObject loadText;

    bool once;
    bool onceElse;


    void Start()
    {

        once = false;
        onceElse = false;
    }

    void Update()
    {
        if(loadText == null)
            loadText = GameObject.Find("LoadingText");

        if (Application.isLoadingLevel)
        {
            if (!once)
            {
                loadText.gameObject.SetActive(true);
                once = true;
            }

        }
        else
        {
            Debug.Log("Here");
            if (!onceElse)
            {
                loadText.gameObject.SetActive(false);
                onceElse = true;
            }
            if (once)
            {
                loadText = null;
                once = false;
                onceElse = false;
            }

        }
    }

}