﻿using UnityEngine;
using System.Collections;

public class Ball : MonoBehaviour
{
    BallMultiMove ballMove;
    Vector3 pos;
    Quaternion rot;
    Vector3 dir;
    float mag;
    float lastRecievedTime;
    // Use this for initialization
    void Start()
    {
        ballMove = GetComponent<BallMultiMove>();
    }

    void SerializeState(PhotonStream stream, PhotonMessageInfo msg)
    {
        if(stream.isWriting == true)
        {
            stream.SendNext(transform.position);
            stream.SendNext(transform.rotation);
            //stream.SendNext(ballMove.magnitude);
            stream.SendNext(rigidbody.velocity.normalized);
        }
        else
        {
            pos = (Vector3)stream.ReceiveNext();
            rot = (Quaternion)stream.ReceiveNext();
            //mag = (float)stream.ReceiveNext();
            dir = (Vector3)stream.ReceiveNext();
            lastRecievedTime = (float)msg.timestamp;
        }
    }

    void OnPhotonSerializeView(PhotonStream stream, PhotonMessageInfo info)
    {
        SerializeState(stream, info);
    }

    // Update is called once per frame
    void Update()
    {
        if(!GetComponent<PhotonView>().isMine)
        {
            ballMove.magnitude = mag;

            float ping = (float)PhotonNetwork.GetPing() * 0.001f;
            float delta = (float)(PhotonNetwork.time - lastRecievedTime);
            float totalTimePassed = ping + delta;

            Vector3 predictedPos = pos + dir * mag * delta;
            
            Vector3 newPos = Vector3.MoveTowards(transform.position, predictedPos, mag * delta);

            if(Vector3.Distance(transform.position,predictedPos) > 2f)
            {
                newPos = predictedPos;
            }
            newPos = pos;
            transform.position = newPos;

            transform.rotation = Quaternion.Slerp(transform.rotation, rot, mag);
        }

    }
}
