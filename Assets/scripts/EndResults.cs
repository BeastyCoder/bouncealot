﻿using UnityEngine;
using UnityEngine.UI;
using System.Collections;

public class EndResults : MonoBehaviour
{
    GameObject obj;
    public Text first;
    public Text second;
    public Text third;
    public Text fourth;
    public Text firstCoin;
    public Text secondCoin;
    public Text thirdCoin;
    public Text fourthCoin;
    public Text stat;
    public Image Foreground;
    public Sprite Loser;
    bool once;
    private int i;
    // Use this for initialization
    void Start()
    {
        obj = GameObject.Find("Ranker");
        i = 0;
        once = false;
        
    }

    // Update is called once per frame
    void Update()
    {
        if(!once)
        {
            if (!obj.GetComponent<PlayerRank>().l[0].IsPlayer)
            {
                Foreground.sprite = Loser;
                stat.text = "Loser";
            }
            PhotonNetwork.LeaveRoom();
            once = true;
        }

        foreach (PlayerInfo p in obj.GetComponent<PlayerRank>().l)
        {
            switch(i)
            {
                case 0:
                    first.text = p.PlayerName;
                    firstCoin.text = p.PlayerCoin.ToString();
                    i++;
                    break;
                case 1:
                    second.text = p.PlayerName;
                    secondCoin.text = p.PlayerCoin.ToString();
                    i++;
                    break;
                case 2:
                    third.text = p.PlayerName;
                    thirdCoin.text = p.PlayerCoin.ToString();
                    i++;
                    break;
                case 3:
                    fourth.text = p.PlayerName;
                    fourthCoin.text = p.PlayerCoin.ToString();
                    i++;
                    break;
            }
        }
    }
}
