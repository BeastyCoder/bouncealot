﻿using UnityEngine;
using UnityEngine.UI;
using System.Collections;

public class AbilityRunner : MonoBehaviour
{
    public Image img;
    GameObject player;
    float elapsedTime;
    float maxTime;
    [SerializeField]
    public int y;
    // Use this for initialization
    void Start()
    {

    }

    public void UseAbility(int x)
    {
        if(x == 1)
        {
            if(player != null)
            {
                if (this.y == 0)
                {
                    player.GetComponent<Juggernaut>().firstAbility = true;
                }
                else if(this.y == 1)
                {
                    player.GetComponent<Expulso>().firstAbility = true;
                }
                else if(this.y == 2)
                {
                    player.GetComponent<Trickster>().firstAbility = true;
                }
                else if (this.y == 3)
                {
                    player.GetComponent<Frostbound>().firstAbility = true;
                }
                else if (this.y == 4)
                {
                    player.GetComponent<Tranquil>().firstAbility = true;
                }
            }
        }
    }

    // Update is called once per frame
    void Update()
    {
       if(player == null)
       {
           try
           {
               foreach (GameObject pl in GameObject.FindGameObjectsWithTag("Player"))
               {
                   if (pl.GetComponent<PhotonView>().isMine)
                   {
                       player = pl;
                       if (player.GetComponent<Juggernaut>()) y = 0;
                       else if (player.GetComponent<Expulso>()) y = 1;
                       else if (player.GetComponent<Trickster>()) y = 2;
                       else if (player.GetComponent<Frostbound>()) y = 3;
                       else if (player.GetComponent<Tranquil>()) y = 4;
                       break;
                   }
               }
               
           }
           catch
           {
               Debug.Log("Nothing found");
           }
       }
       else
       {
           
           if (y == 0)
           {
               elapsedTime = player.GetComponent<Juggernaut>().firstElapsedTime;
               maxTime = player.GetComponent<Juggernaut>().firstCD;
           }
           else if (y == 1)
           {
               elapsedTime = player.GetComponent<Expulso>().firstElapsedTime;
               maxTime = player.GetComponent<Expulso>().firstCD;
           }
           else if(y == 2)
           {
               elapsedTime = player.GetComponent<Trickster>().firstElapsedTime;
               maxTime = player.GetComponent<Trickster>().firstCD;
           }
           else if (y == 3)
           {
               elapsedTime = player.GetComponent<Frostbound>().firstElapsedTime;
               maxTime = player.GetComponent<Frostbound>().firstCD;
           }
           else if (y == 4)
           {
               elapsedTime = player.GetComponent<Tranquil>().firstElapsedTime;
               maxTime = player.GetComponent<Tranquil>().firstCD;
           }
           
           img.fillAmount = 1 - (elapsedTime / maxTime);
       }
    }
}
