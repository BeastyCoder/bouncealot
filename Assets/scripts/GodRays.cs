﻿using UnityEngine;
using System.Collections;

public class GodRays : MonoBehaviour
{
    [RPC]
    void SetSpawn(bool x)
    {
        GameObject.Find("Global").GetComponent<RayScript>().once = x;
    }
    // Use this for initialization
    void Start()
    {
        GetComponent<PhotonView>().RPC("SetSpawn", PhotonTargets.Others, new object[] { });
    }

    void OnTriggerEnter(Collider col)
    {
        if(col.gameObject.tag == "Player")
        {
            if(col.gameObject.GetComponent<PhotonView>().isMine)
            {
                
                GameObject.FindGameObjectWithTag("arrow").GetComponent<Arrow>().maze = false;
                col.gameObject.transform.position = new Vector3(0, 10, 0);
                GameObject.Find("Global").GetComponent<Objective>().ChangeObjective("Collect 100 coins");
            }
        }
    }

    // Update is called once per frame
    void Update()
    {

    }
}
