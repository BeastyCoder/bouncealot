﻿using UnityEngine;
using UnityEngine.UI;
using System.Collections;

public class JuggernautRun : MonoBehaviour
{
    public Image img;
    GameObject player;
    float elapsedTime;
    float maxTime;
    // Use this for initialization
    void Start()
    {

    }

    public void UseAbility(int x)
    {
        if (x == 1)
        {
            if (player != null)
            {
                player.GetComponent<Tranquil>().firstAbility = true;
            }
        }
    }

    // Update is called once per frame
    void Update()
    {
        if (player == null)
        {
            try
            {
                player = GameObject.FindGameObjectWithTag("Player");
            }
            catch
            {
                Debug.Log("Nothing found");
            }
        }
        else
        {
            elapsedTime = player.GetComponent<Tranquil>().firstElapsedTime;
            maxTime = player.GetComponent<Tranquil>().firstCD;
            img.fillAmount = 1 - (elapsedTime / maxTime);
        }
    }
}
