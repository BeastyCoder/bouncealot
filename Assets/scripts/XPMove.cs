﻿using UnityEngine;
using System.Collections;

public class XPMove : MonoBehaviour
{
    GameObject[] obj;
    System.Random rand = new System.Random();
    public bool moving = false;
    // Use this for initialization
    void Start()
    {
        obj = GameObject.FindGameObjectsWithTag("SpawnPoint");
        moving = false;
    }

    void OnTriggerExit(Collider col)
    {
        if(col.gameObject.tag == "ground")
        {
            transform.position = new Vector3(transform.position.x, transform.position.y - 0.5f, transform.position.z);
        }
    }

    // Update is called once per frame
    void Update()
    {
        if (!moving)
        {
            Vector3 pos = obj[rand.Next(0, obj.Length - 1)].transform.position;
            
            GetComponent<Actor>().MoveOrder(pos);
            moving = true;
        }
    }
}
