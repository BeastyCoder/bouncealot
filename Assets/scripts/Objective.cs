﻿using UnityEngine;
using UnityEngine.UI;
using System.Collections;

public class Objective : MonoBehaviour
{
	public string objText;
	public GameObject txt;
	public bool change;
	// Use this for initialization
	void Start()
	{
		objText = "Collect 100 coins";

		change = true;
		ChangeObjective (objText);
	}
	
	public void ChangeObjective(string obj)
	{
		CancelInvoke ("dactivate");
		txt.gameObject.SetActive (true);
		objText = obj;
		change = true;
		Invoke ("dactivate",40);
	}
	
	void dactivate(){
		txt.gameObject.SetActive (false);
	}
	
	// Update is called once per frame
	void Update()
	{
		if(change && GameObject.Find("Join").GetComponent<join>().spawned)
		{
			txt.GetComponentInChildren<Text>().text = objText;
			change = false;
		}
	}
}