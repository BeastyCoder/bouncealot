﻿using UnityEngine;
using UnityEngine.UI;
using System.Collections;

public class CoinDisplay : MonoBehaviour
{
    public Text txt;
    GameObject player;
    int coins;
    // Use this for initialization
    void Start()
    {

    }

    // Update is called once per frame
    void Update()
    {
        if(player == null)
        {
            try
            {          
                foreach(GameObject pl in GameObject.FindGameObjectsWithTag("Player"))
                {
                    if(pl.gameObject.GetComponent<PhotonView>().isMine)
                    {
                        player = pl;
                    }
                }
            }
            catch
            {
                
            }
        }
        else
        {
            coins = player.GetComponent<PlayerCoin>().totalCoin;
            txt.text = coins.ToString();
        }
    }
}
