﻿using UnityEngine;
using System.Collections;

public class SoundPlay : MonoBehaviour
{
    public AudioClip coin;
    public AudioClip button;
    public void PlayCoin()
    {
        audio.PlayOneShot(coin);
    }

    public void PlayButton()
    {
        audio.PlayOneShot(button);
    }
    // Use this for initialization
    void Start()
    {

    }

    // Update is called once per frame
    void Update()
    {

    }
}
