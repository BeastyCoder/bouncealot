﻿using UnityEngine;
using System.Collections;

public class PlayerInfo
{
    public string PlayerName { get; set; }
    public int PlayerCoin { get; set; }
    public int PlayerId { get; set; }
    public int PlayerXP { get; set; }
    public int PlayerLevel { get; set; }
    public bool IsPlayer { get; set; }
}
