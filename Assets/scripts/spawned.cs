﻿using UnityEngine;
using System.Collections;

public class spawned : MonoBehaviour
{

    private int i = 1;
    
    // Use this for initialization
    void Start()
    {
        GetComponent<PhotonView>().RPC("SetSpawn", PhotonTargets.All, new object[] { false });
    }

    [RPC]
    void SetSpawn(bool x)
    {
        GameObject.Find("Global").GetComponent<CoinSpawningSystem>().spawn = x;
    }

    [RPC]
    void DestroyCoin()
    {
        PhotonNetwork.Destroy(GetComponent<PhotonView>());
        GameObject.Find("Global").GetComponent<CoinSpawningSystem>().spawn = true;
    }

    void OnTriggerEnter(Collider col)
    {
        if (col.gameObject.tag == "Player")
        {
            if (col.gameObject.GetComponent<PhotonView>().ownerId == PhotonNetwork.player.ID)
            {

                GameObject.Find("Global").GetComponent<SoundPlay>().PlayCoin();
                i = GameObject.Find("Global").GetComponent<CounterCoin>().CounterCombo(col.gameObject.GetComponent<PhotonView>().ownerId);

                col.gameObject.GetComponent<PlayerCoin>().AddCoin(i);
                GetComponent<PhotonView>().RPC("SetSpawn", PhotonTargets.All, new object[] { true });
                GetComponent<PhotonView>().RPC("DestroyCoin",PhotonTargets.MasterClient,new object[] {});
               
            }
            
        }
    }

    // Update is called once per frame
    void Update()
    {

    }
}
