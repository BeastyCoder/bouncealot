﻿using UnityEngine;
using System.Collections;

public class BuffSpawner : MonoBehaviour
{

    GameObject[] obj;
    public GameObject buffObj;
    System.Random rand = new System.Random();
    public bool spawn = true;
    public float elapsedTime = 0.0f;
    
    public int wormIndex = -1;
    // Use this for initialization
    void Start()
    {
        obj = GameObject.FindGameObjectsWithTag("SpawnPoint");
    }

    // Update is called once per frame
    void Update()
    {
        if (PhotonNetwork.isMasterClient)
        {
            if (elapsedTime < 60f)
            {
                elapsedTime += Time.deltaTime;
                
            }
            
            
            else if (spawn == true)
            {
                int i = rand.Next(0, obj.Length - 1);
                while (i == wormIndex)
                {
                    i = rand.Next(0, obj.Length - 1);
                }
                Vector3 pos = obj[i].transform.position;
                GetComponent<SpawnWormhole>().buffIndex = i;
                
                PhotonNetwork.InstantiateSceneObject(buffObj.name, pos, Quaternion.identity, 0, null);
                spawn = false;
            }
        }
    }
}
