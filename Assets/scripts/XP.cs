﻿using UnityEngine;
using UnityEngine.UI;
using System.Collections;

public class XP : MonoBehaviour
{
    public Image xpbar;
    private int curXP;
    private int maxXP = 100;
    public int level = 1;
    private GameObject player;
    public float elapsedTime = 0.0f;
    public bool xpEnabled = true;
    
    // Use this for initialization
    void Start()
    {

    }


    // Update is called once per frame
    void Update()
    {
        if (player == null)
        {
            try
            {
                foreach(GameObject pl in GameObject.FindGameObjectsWithTag("Player"))
                {
                    if(pl.GetComponent<PhotonView>().isMine)
                    {
                        player = pl;
                    }
                }
            }
            catch
            {

            }
        }
        else
        {
            curXP = player.GetComponent<PlayerXP>().curXP;
            maxXP = player.GetComponent<PlayerXP>().maxXP;
            level = player.GetComponent<PlayerXP>().level;
            xpbar.fillAmount = curXP / (maxXP * 1.0f);
            xpbar.GetComponentInChildren<Text>().text = level.ToString();
        }
    }
}
