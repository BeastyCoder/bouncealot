﻿using UnityEngine;
using System.Collections;

public class RayScript : MonoBehaviour
{
    GameObject[] obj;
    bool translate = true;
    System.Random rand = new System.Random();
    GameObject ray;
    float elapsedTime = 0.0f;
    public int hazeIndex = -1;
    public bool once;
    // Use this for initialization
    void Start()
    {
        obj = GameObject.FindGameObjectsWithTag("mazespawn");
        once = false;
    }

    // Update is called once per frame
    void Update()
    {
        if (PhotonNetwork.isMasterClient && !once)
        {
            int i = rand.Next(0, obj.Length - 1);
            ray = PhotonNetwork.InstantiateSceneObject("GodRays", obj[i].transform.position, Quaternion.identity, 0, null);
            translate = false;
            elapsedTime = 0.0f;
            hazeIndex = i;
            once = true;
        }
        else if(translate && once)
        {
            
            int i = rand.Next(0, obj.Length - 1);
            ray.transform.position = obj[i].transform.position;
            translate = false;
            elapsedTime = 0.0f;
            hazeIndex = i;
        }
        elapsedTime += Time.deltaTime;

        if(elapsedTime > 180f)
        {
            
            translate = true;
        }

    }
}
