﻿using UnityEngine;
using System.Collections;

public class Trickster : MonoBehaviour
{
    public bool firstAbility;
    public float firstCD = 20f;
    public float firstElapsedTime;
    public bool onCD;
    public bool once;
    public Texture prevTex;
    public Texture invisibleTex;

    [RPC]
    void Invisibility(bool x)
    {
        if (!GetComponent<PhotonView>().isMine)
            GetComponent<MeshRenderer>().enabled = x;
    }
    // Use this for initialization
    void Start()
    {
        firstAbility = false;
        firstElapsedTime = firstCD;
        onCD = false;
        once = false;
    }

    // Update is called once per frame
    void Update()
    {
        if (firstAbility)
        {
            
            if (!once)
            {
                firstElapsedTime = 0.0f;
                prevTex = GetComponent<MeshRenderer>().material.mainTexture;
                GetComponent<MeshRenderer>().material.mainTexture = invisibleTex;
                once = true;
            }
            if (firstElapsedTime < firstCD)
            {
                firstElapsedTime += Time.deltaTime;
                if (firstElapsedTime > 10.0f)
                {
                    GetComponent<PhotonView>().RPC("Invisibility", PhotonTargets.All, new object[] { true });
                    GetComponent<MeshRenderer>().material.mainTexture = prevTex;
                }
                else
                {
                    Debug.Log("Invisible");
                    GetComponent<PhotonView>().RPC("Invisibility", PhotonTargets.All, new object[] { false });
                }
            }
            else
            {
                firstElapsedTime = firstCD;
                firstAbility = false;
                once = false;
            }
        }
    }
}
