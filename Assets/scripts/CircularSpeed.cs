﻿using UnityEngine;
using UnityEngine.UI;
using System.Collections;

public class CircularSpeed : MonoBehaviour
{
    public Image speedImg;
    private GameObject player;
    public float maxSpeed;
    private float curSpeed;
    private string curSpeedString;
    // Use this for initialization
    void Start()
    {

    }



    // Update is called once per frame
    void Update()
    {
        if (player == null)
        {
            try
            {
                foreach (GameObject pl in GameObject.FindGameObjectsWithTag("Player"))
                {
                    if (pl.GetComponent<PhotonView>().isMine)
                    {
                        player = pl;
                    }
                }
            }
            catch
            {

            }
        }
        else
        {
            maxSpeed = player.GetComponent<BallMultiMove>().maxSpeed;
            curSpeed = player.GetComponent<BallMultiMove>().magnitude;
            speedImg.fillAmount = curSpeed / maxSpeed;
            curSpeedString = Mathf.Floor(curSpeed).ToString();
            speedImg.GetComponentInChildren<Text>().text = curSpeedString;
        }
    }
}
