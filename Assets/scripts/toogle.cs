﻿using UnityEngine;
using UnityEngine.UI;
using System.Collections;

public class toogle : MonoBehaviour
{
    private GameObject gObj;
    // Use this for initialization
    void Awake()
    {
        gObj = GameObject.Find("Sensitivity");
    }

    void Start()
    {
        
    }

    public void Toggle()
    {
        gObj.SetActive(!GameObject.Find("Toggle").GetComponent<Toggle>().isOn);
    }

    // Update is called once per frame
    void Update()
    {
        gObj.SetActive(!GameObject.Find("Toggle").GetComponent<Toggle>().isOn);
    }
}
