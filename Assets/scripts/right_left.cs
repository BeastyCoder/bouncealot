﻿using UnityEngine;
using System.Collections;

public class right_left : MonoBehaviour {

	int loop_helper=0;
	Vector3 initial;
	public int direction ;
	public float range;
    public float speed;
	// Use this for initialization
	void Start () {
		initial = transform.position;
        speed = 1f;
	}
	
	// Update is called once per frame
	void Update () {
		if (direction == 0 && transform.position.z < (initial.z + range)) {
			transform.Translate (Vector3.forward  * speed * Time.deltaTime);
			if(transform.position.z>=range)direction=1;
		}
		else if (direction == 1 && transform.position.z > (initial.z - range)) {
			transform.Translate (Vector3.back * speed *Time.deltaTime);
			if(transform.position.z<=-range)direction=0;
		}
		//Debug.Log ( transform.position.z );
	}
}
