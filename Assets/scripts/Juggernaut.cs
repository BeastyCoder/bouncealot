﻿using UnityEngine;
using System.Collections;

public class Juggernaut : MonoBehaviour
{
    public bool firstAbility = false;
    
    public float firstCD = 8f;
    public float firstElapsedTime;
    private bool once = false;

    // Use this for initialization
    void Start()
    {
        firstElapsedTime = firstCD;
    }

    // Update is called once per frame
    void Update()
    {
        if (firstAbility)
        {
            if(!once)
            {
                rigidbody.constraints = RigidbodyConstraints.FreezePositionY;
                rigidbody.AddForce(rigidbody.velocity.normalized * GetComponent<BallMultiMove>().magnitude * 500f);
                firstElapsedTime = 0.0f;
                once = true;
            }
            if (firstElapsedTime < firstCD)
            {
                firstElapsedTime += Time.deltaTime;
                if (firstElapsedTime > 2.0f)
                    rigidbody.constraints = RigidbodyConstraints.None;
            }
            else
            {
                firstElapsedTime = firstCD;
                firstAbility = false;
                once = false;
            }
        }
    }
}
