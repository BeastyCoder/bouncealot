﻿using UnityEngine;
using UnityEngine.UI;
using System.Collections;

public class Login : MonoBehaviour
{
    GameObject j;
    public InputField username;
    public InputField password;
    public GameObject toggle;
    public GameObject alertBox;
    // Use this for initialization
    void Start()
    {
        
        j = GameObject.Find("Join");
        if(PlayerPrefs.HasKey("autoLog"))
        {
            if(PlayerPrefs.GetInt("autoLog") == 1)
            {
                username.text = PlayerPrefs.GetString("authName");
                password.text = PlayerPrefs.GetString("authToken");
                toggle.GetComponent<Toggle>().isOn = true;
                login();
            }
        }
        else
        {
            PlayerPrefs.SetInt("autoLog", 0);
        }
    }

    public void log_in()
    {
        Invoke("login", 0.7f);
    }

    void login()
    {
        if (username.text != "" && password.text != "")
        {
            if(toggle.GetComponent<Toggle>().isOn)
            {
                save();
            }
            j.GetComponent<join>().authName = username.text;
            j.GetComponent<join>().authToken = password.text;
            j.GetComponent<join>().connect();
        }
    }

    public void save()
    {
        PlayerPrefs.SetString("authName", username.text);
        PlayerPrefs.SetString("authToken", password.text);
        PlayerPrefs.SetInt("autoLog", toggle.GetComponent<Toggle>().isOn ? 1 : 0);
    }

    public void SignUp()
    {
        Invoke("sign_up", 0.7f);
    }

    void sign_up()
    {
        Application.OpenURL("http://bouncealot.xerothermal.com");
    }

    public void LoginFailed()
    {
        alertBox.gameObject.SetActive(true);
        toggle.GetComponent<Toggle>().isOn = false;
    }

    // Update is called once per frame
    void Update()
    {

    }
}
