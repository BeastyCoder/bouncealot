﻿using UnityEngine;
using System.Collections;

public class CameraManager : MonoBehaviour
{
    public Camera cam1;
    public Camera cam2;
    public bool enabled;
    public float elapsedTime;
    // Use this for initialization
    void Start()
    {
        enabled = true;
        elapsedTime = 0.0f;
    }

    // Update is called once per frame
    void Update()
    {
        if(enabled)
        {
            if(cam2.gameObject.GetActive())
            {
                foreach(GameObject pl in GameObject.FindGameObjectsWithTag("Player"))
                {
                    if (pl.GetComponent<PhotonView>().isMine)
                        pl.rigidbody.constraints = RigidbodyConstraints.None;
                }
            }
            cam2.gameObject.SetActive(false);
            cam1.gameObject.SetActive(true);
            elapsedTime = 0.0f;
        }
        else
        {
            cam1.GetComponent<CameraBehave>().awayDistance = 0.0f;
            cam1.gameObject.SetActive(false);
            cam2.gameObject.SetActive(true);
            elapsedTime += Time.deltaTime;
            if (elapsedTime > 9f)
                enabled = true;
        }
    }
}
