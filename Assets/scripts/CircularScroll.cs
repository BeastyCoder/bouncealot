﻿using UnityEngine;
using UnityEngine.UI;
using System.Collections;
using System.Collections.Generic;

public class CircularScroll : MonoBehaviour
{
    public GameObject platform;
    public GameObject[] prefabs;
    public float radius;
    public float angularDiff;
    public Text nameText;
    public Text typeText;
    public Text abilityText;
    public int i;
    public int curIndex;

    bool right;
    bool left;
    bool once;

    private Vector2 fingerStart;
    private Vector2 fingerEnd;
    private float curAngle;
    private bool input;
    private List<GameObject> objects;
    private GameObject mCam;
    public stack s;
    
    // Use this for initialization
    void Start()
    {
        angularDiff = 2 * 180 / prefabs.Length;
        i = 0;
        once = false;
        objects = new List<GameObject>();
        input = true;
        right = false;
        left = false;
        curAngle = 0.0f;
        curIndex = -1;
        mCam = GameObject.Find("menuCam");
        mCam.gameObject.SetActive(false);
        s = new stack();
    }

    void MoveRight()
    {
        platform.transform.Rotate(new Vector3(0, -angularDiff*Time.deltaTime, 0));
        curAngle += angularDiff * Time.deltaTime;
        
    }

    void MoveLeft()
    {
        platform.transform.Rotate(new Vector3(0, angularDiff*Time.deltaTime, 0));
        curAngle += angularDiff * Time.deltaTime;
        
    }

    public void SelectAvatar()
    {
        Destroy(GameObject.Find("menuCam"));

        GameObject.Find("Join").GetComponent<join>().SetAvatar(i);
        GameObject.Find("Join").GetComponent<join>().join_or_create_room();
    }

    // Update is called once per frame
    void Update()
    {
        if (Input.GetKeyDown(KeyCode.Escape))
        {
            s.start_stack();
            if (mCam)
                mCam.SetActive(true);
            Application.LoadLevel("main");

        }

        if(Input.GetKeyDown(KeyCode.A) && input)
        {
            input = false;
            right = true;
            i--;
            if (i < 0) i = 4;
        }
        
        if (Input.GetKeyDown(KeyCode.D) && input)
        {
            input = false;
            left = true;
            i = (i + 1) % objects.Count; 
        }

        if (!once)
        {
            for (int i = 0; i < prefabs.Length; i++)
            {
                float angle = 2 * Mathf.PI * i / prefabs.Length;
                Vector3 pos = new Vector3(Mathf.Cos(angle)*radius + 2, 8.0f, Mathf.Sin(angle)*radius + 2);
                GameObject o = Instantiate(prefabs[i], pos, Quaternion.identity) as GameObject;
                o.transform.parent = platform.transform;
                objects.Add(o);
            }
            
            //Debug.Log(objects[0]);
            
            once = true;
        }

        
        foreach(GameObject o in objects)
        {
            o.transform.Rotate(new Vector3(0, 10 * Time.deltaTime, 10*Time.deltaTime));

            if(curIndex != i && input)
            {
                //Debug.Log(i);
                nameText.text = objects[i].GetComponent<AvatarInfo>().Name;
                typeText.text = objects[i].GetComponent<AvatarInfo>().Type;
                abilityText.text = objects[i].GetComponent<AvatarInfo>().Ability;
                curIndex = i;
            }
        }

        if(Input.touchCount > 0 && input)
        {
            foreach(Touch t in Input.touches)
            {
                //Debug.Log("touch");
                if(t.phase == TouchPhase.Began)
                {
                    //Debug.Log("Begin");
                    fingerStart = t.position;
                    fingerEnd = t.position;
                }
                if(t.phase == TouchPhase.Moved)
                {
                    //Debug.Log("Moved");
                    fingerEnd = t.position;
                    Vector2 delta = fingerEnd - fingerStart;
                    if(delta.x > 0.2f)
                    {
                        //Debug.Log("positive");
                        input = false;
                        right = true;
                        i--;
                        if (i < 0) i = 4;
                    }
                    else if(delta.x < -0.2f)
                    {
                        //Debug.Log("negative");
                        input = false;
                        left = true;
                        i = (i + 1) % objects.Count;                     
                    }
                }
            }
        }
        else
        {
            if (curAngle+angularDiff*Time.deltaTime < angularDiff)
            {
                if (right)
                {
                    MoveRight();
                }
                else if (left)
                {
                    MoveLeft();
                }
            }
            else
            {
                float additionalRot = angularDiff - curAngle;
                if (right) platform.transform.Rotate(new Vector3(0, -additionalRot, 0));
                else if (left) platform.transform.Rotate(new Vector3(0, additionalRot, 0));
                curAngle = 0.0f;
                input = true;
                right = false;
                left = false;
            }
        }

        
    }
}
