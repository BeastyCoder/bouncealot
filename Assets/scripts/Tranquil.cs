﻿using UnityEngine;
using System.Collections;

public class Tranquil: MonoBehaviour
{
    public bool firstAbility = false;
    bool secondAbility = false;
    public float firstElapsedTime = 0.0f;
    float secondElpasedTime = 0.0f;
    public float firstCD = 10f;
    float secondCD = 50f;
    // Use this for initialization
    void Start()
    {

    }

    // Update is called once per frame
    void Update()
    {
        if(firstAbility)
        {
            if(firstElapsedTime < firstCD)
            {
                firstElapsedTime += Time.deltaTime;
                if(firstElapsedTime < 3.0f)
                {
                    rigidbody.constraints = RigidbodyConstraints.FreezePositionY;
                }
                else
                {
                    rigidbody.constraints = RigidbodyConstraints.None;
                }
            }
            else
            {
                firstElapsedTime = 0.0f;
                firstAbility = false;
            }
        }
    }
}
