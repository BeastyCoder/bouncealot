﻿using UnityEngine;
using System.Collections;

public class Death : MonoBehaviour
{
    public bool death = false;
    public bool once;
    public GameObject obj;
    
    [RPC]
    void Add_XP(int id, int l)
    {
        foreach(GameObject pl in GameObject.FindGameObjectsWithTag("Player"))
        {
            if (pl.GetComponent<PhotonView>().isMine && pl.GetComponent<PhotonView>().ownerId == id)
            {
                int x = 1;
                if(l >= pl.gameObject.GetComponent<PlayerXP>().level)
                {
                    x = l + 1 - pl.gameObject.GetComponent<PlayerXP>().level;
                }
                pl.gameObject.GetComponent<PlayerXP>().AddXP(100 * (x));
            }
        }
    }

    // Use this for initialization
    void Start()
    {
        once = false;
    }

    // Update is called once per frame
    void Update()
    {
        if (transform.position.y < -20 || Mathf.Abs(transform.position.x) > 500f || transform.position.y > 570f) death = true;

        if (death && !once)
        {
            Debug.Log("Entered");
            rigidbody.constraints = RigidbodyConstraints.FreezeAll;
            GameObject g = Instantiate(obj, transform.position, Quaternion.identity) as GameObject;
            g.GetComponent<DeathWarp>().animStart = true;
            g.GetComponent<DeathWarp>().id = GetComponent<PhotonView>().ownerId;
            GetComponent<PlayerCoin>().AddCoin(-10);
            gameObject.GetComponent<PhotonView>().RPC("Add_XP", PhotonTargets.Others, new object[] { GetComponent<LastHit>().id, GetComponent<PlayerXP>().level });
            once = true;
        }
    }
}
