﻿using UnityEngine;
using System.Collections;

public class Wormhole : MonoBehaviour
{
    float elapsedTime = 0.0f;
    System.Random rand = new System.Random();
    GameObject[] obj;
    public int hazeIndex = -1;
    public GameObject pl;
    public GameObject arrow;
    public bool animStart;
    public bool animEnd;
    public float animTime;
    public AudioClip whoosh;
    // Use this for initialization
    void Start()
    {
        elapsedTime = 0.0f;
        obj = GameObject.FindGameObjectsWithTag("mazespawn");
        animStart = false;
        animEnd = false;
        animTime = 0.0f;
        GetComponent<PhotonView>().RPC("SetSpawn", PhotonTargets.All, new object[] { false });
    }

    [RPC]
    void SetSpawn(bool x)
    {
        GameObject.Find("Global").GetComponent<SpawnWormhole>().spawn = x;
    }

    [RPC]
    void DestroyWorm()
    {
        GameObject.Find("Global").GetComponent<SpawnWormhole>().spawn = true;
        GameObject.Find("Global").GetComponent<SpawnWormhole>().elapsedTime = 0.0f;
        PhotonNetwork.Destroy(GetComponent<PhotonView>());  
    }

    void OnTriggerEnter(Collider col)
    {
        if(col.gameObject.tag == "Player")
        {
            if (col.gameObject.GetComponent<PhotonView>().ownerId == PhotonNetwork.player.ID)
            {
                col.gameObject.GetComponent<Wormholed>().collided = true;
                pl = col.gameObject;
                animStart = true;
                audio.PlayOneShot(whoosh);
                
            }

        }
    }

    // Update is called once per frame
    void Update()
    {
        hazeIndex = GameObject.Find("Global").GetComponent<RayScript>().hazeIndex;
        if (!animStart)
        {
            if (elapsedTime < 40.0f)
            {
                elapsedTime += Time.deltaTime;
            }
            else
            {
                GetComponent<PhotonView>().RPC("SetSpawn", PhotonTargets.All, new object[] { true });
                GetComponent<PhotonView>().RPC("DestroyWorm", PhotonTargets.MasterClient, new object[] { });
            }
            if (elapsedTime > 0.5f)
            {
                if (!gameObject.GetComponent<BoxCollider>().enabled) gameObject.GetComponent<BoxCollider>().enabled = true;
            }
            
        }
        else if(!animEnd)
        {
            //GetComponent<PhotonView>().RPC("DeactivateCollider", PhotonTargets.All, new object[] { });
            pl.rigidbody.constraints = RigidbodyConstraints.FreezeAll;
            pl.GetComponent<MeshRenderer>().enabled = false;
            if(animTime < 2.0f)
            {
                gameObject.GetComponentInChildren<ParticleEmitter>().minSize -= (Time.deltaTime*10);
                gameObject.GetComponentInChildren<ParticleEmitter>().maxSize -= (Time.deltaTime*10);
                animTime += Time.deltaTime;
            }
            else
            {
                animEnd = true;               
            }
        }
        else
        {
            int i = rand.Next(0, obj.Length - 1);
            while (i == hazeIndex)
            {
                i = rand.Next(0, obj.Length - 1);
            }
            Vector3 pos = obj[i].transform.position;
            pos.y += 1f;
            pl.gameObject.transform.position = pos;
            pl.rigidbody.constraints = RigidbodyConstraints.None;
            GameObject.Find("Global").GetComponent<Objective>().ChangeObjective("Follow the arrow");
            pl.GetComponent<MeshRenderer>().enabled = true;
            GameObject.Find("Global").GetComponent<SpawnWormhole>().spawn = true;
            GameObject.Find("Global").GetComponent<SpawnWormhole>().elapsedTime = 0.0f;
            GameObject.FindGameObjectWithTag("arrow").GetComponent<Arrow>().maze = true;
            pl.GetComponent<Wormholed>().collided = false;
            GetComponent<PhotonView>().RPC("DestroyWorm", PhotonTargets.MasterClient, new object[] { });
        }
    }
}
