﻿using UnityEngine;
using UnityEngine.UI;
using System.Collections;

public class AnchorName : MonoBehaviour
{
    public Canvas c;
    public GameObject obj;
    bool once = false;
    public GameObject[] names = new GameObject[30];
	string[] substr = new string[30];
    // Use this for initialization
    void Start()
    {

    }

    // Update is called once per frame
    void Update()
    {


        foreach (GameObject pl in GameObject.FindGameObjectsWithTag("Player"))
        {
            if (names[pl.GetComponent<PhotonView>().ownerId] == null && !pl.GetComponent<PhotonView>().isMine)
            {
                Vector3 pos = GetScreenPosition(pl.transform, c);
                GameObject o = Instantiate(obj.gameObject) as GameObject;

                o.transform.parent = c.transform;
                o.transform.position = new Vector3(pos.x + Screen.width / 10, pos.y + Screen.height / 10);
                o.GetComponentInChildren<Text>().text = pl.GetComponent<PhotonView>().owner.name;
				o.GetComponentInChildren<Text>().color = new Color32(62,105,5,255);
				o.GetComponentInChildren<Text>().text += " " + pl.GetComponent<PlayerCoin>().totalCoin;
                names[pl.GetComponent<PhotonView>().ownerId] = o;
				//Debug.Log(pl.GetComponent<PhotonView>().owner.name);
				if(pl.GetComponent<PhotonView>().owner.name.Length>10) substr[pl.GetComponent<PhotonView>().ownerId] = pl.GetComponent<PhotonView>().owner.name.Substring(0,9);
				else{
					substr[pl.GetComponent<PhotonView>().ownerId]=pl.GetComponent<PhotonView>().owner.name;
				}
			}
            else
            {
                if (!pl.GetComponent<PhotonView>().isMine)
                {
                    if(pl.GetComponent<MeshRenderer>().enabled == false)
                    {
                        names[pl.GetComponent<PhotonView>().ownerId].gameObject.SetActive(false);
                    }
                    else
                    {
                        names[pl.GetComponent<PhotonView>().ownerId].gameObject.SetActive(true);
                    }
                    
                    Vector3 pos = GetScreenPosition(pl.transform, c);
					names[pl.GetComponent<PhotonView>().ownerId].GetComponentInChildren<Text>().text = substr[pl.GetComponent<PhotonView>().ownerId];
					names[pl.GetComponent<PhotonView>().ownerId].GetComponentInChildren<Text>().color = new Color32(62,105,5,255);
					names[pl.GetComponent<PhotonView>().ownerId].GetComponentInChildren<Text>().text += " " + pl.GetComponent<PlayerCoin>().totalCoin;
                    names[pl.GetComponent<PhotonView>().ownerId].transform.position = new Vector3(pos.x + 50f, pos.y + 50f);
                }
            }

        }

        for(int i = 0; i < names.Length; i++)
        {
            bool found = false;
            if(names[i] != null)
            {
                foreach(GameObject o in GameObject.FindGameObjectsWithTag("Player"))
                {
                    if (o.gameObject.GetComponent<PhotonView>().ownerId == i)
                    {
                        found = true;
                        break;
                    }
                }
                if(!found)
                {
                    Destroy(names[i]);
                    names[i] = null;
                }
            }
        }

    }

    public Vector3 GetScreenPosition(Transform transform, Canvas canvas)
    {
        Vector3 pos;
        float width = canvas.GetComponent<RectTransform>().sizeDelta.x;
        float height = canvas.GetComponent<RectTransform>().sizeDelta.y;
        float x = Camera.main.WorldToScreenPoint(transform.position).x;
        float y = Camera.main.WorldToScreenPoint(transform.position).y;
        pos = new Vector3(x, y);
        return pos;
    }
}
