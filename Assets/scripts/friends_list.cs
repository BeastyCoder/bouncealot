﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;
public class friends_list : MonoBehaviour {

	// Use this for initialization
	public string total="total_friends";
	public string friend_id="friend_id_";


	public string[] friends_arr(){
		int friend_len = PlayerPrefs.GetInt (total);
		string [] fr_arr=new string[friend_len];

		for (int i=0; i<friend_len; i++) {
			int id=i+1;
			fr_arr[i]=PlayerPrefs.GetString(friend_id+id);
		}
		return fr_arr;
	}

	public void add_friend(string name){

		int prev_total = PlayerPrefs.GetInt (total);
		string [] fr_arr = friends_arr ();

		if (System.Array.IndexOf (fr_arr, name) < 0) {/// a new name
			int new_total=prev_total+1;

			PlayerPrefs.SetInt(total,new_total);// just increaed the len
			PlayerPrefs.SetString(friend_id+new_total,name);
		}

	}
	public void del_friend(string name){
		int prev_total = PlayerPrefs.GetInt (total);
		string [] fr_arr = friends_arr (); /// string indx and friend_id not same
		
		if (System.Array.IndexOf (fr_arr,name) >= 0) {/// name exists. so delete it

			int index=System.Array.IndexOf (fr_arr,name);
			int new_total=prev_total-1;

			for(int i=index;i<prev_total-1;i++){
				int nxt_id=i+1;
				string nxt=PlayerPrefs.GetString(friend_id+nxt_id);
				PlayerPrefs.SetString(friend_id+i,nxt);
			}

		}
	}
	public void friends_now_update(){

		string [] friend_list = friends_arr ();
		PhotonNetwork.FindFriends (friend_list);

	}
	public List<FriendInfo> get_updated_friends(){
        Debug.Log(PhotonNetwork.Friends);
        List<FriendInfo> friends = PhotonNetwork.Friends;
        Debug.Log(friends[0]);
        foreach (FriendInfo i in friends)
        {
            Debug.Log(i.Name + " " + i.IsOnline + " " + i.IsInRoom + " " + i.Room);
        }
        return friends;

	}

    public int get_total_friends()
    {
        int friend_len = PlayerPrefs.GetInt(total);
        return friend_len;
    }

}
