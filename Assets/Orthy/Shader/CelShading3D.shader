﻿Shader "basicShader/CelShading3D"{
	Properties{
		_Color ("Color Tint",Color) = (1.0,1.0,1.0,1.0)
		_UnlitColor ("UnlitColor", Color) = (0.0,0.0,0.0,0.0)
		_OutlineColor ("OutlineColor",Color) = (0.0,0.0,0.0,0.0)
		_DiffuseThreshold ("DiffuseThreshold",Range(0,1)) = 0.3
		_OutlineThickness ("OutlineThickness", Range(0,0.5)) = 0.01
		_MainTex("Diffuse Texture",2D) = "white"{}
	}
	
	SubShader{
		
		Pass{
			Tags{"LightMode" = "ForwardBase"}
			Cull Back
			Lighting on
			CGPROGRAM
			#pragma vertex vert
			#pragma fragment frag
			
			uniform float4 _Color;
			uniform float4 _UnlitColor;
			uniform float4 _LightColor0;
			uniform float4 _OutlineColor;
			uniform float4 _MainTex_ST;
			uniform float4 _LitColor;
			uniform float _DiffuseThreshold;
			uniform float _OutlineThickness;
			uniform sampler2D _MainTex;
			
			struct vertexInput{
				float4 pos : POSITION;
				float3 norm : NORMAL;
				float3 tang : TANGENT;
				float4 texCo : TEXCOORD0;
			};
			
			struct vertexOutput{
				float4 posOut : SV_POSITION;
				float4 posWorld : TEXCOORD0;
				float3 normalDir : TEXCOORD1;
				float3 tangDir : TEXCOORD2;
				float4 tex : TEXCOORD3;
			};
			
			vertexOutput vert(vertexInput i){
				vertexOutput o;
				o.posOut = mul(UNITY_MATRIX_MVP,i.pos);
				o.posWorld = mul(i.pos,_Object2World);
				o.normalDir = normalize(mul(float4(i.norm,1.0),_World2Object).xyz);
				o.tangDir = normalize(mul(float4(i.tang,1.0),_World2Object).xyz);
				o.tex = i.texCo;
				return o;
			}
			
			float4 frag(vertexOutput i):COLOR{
				float3 normalDirection = normalize(i.normalDir);
				float3 viewDirection = normalize(_WorldSpaceCameraPos - i.posWorld.xyz);
				float3 lightDir;
				float3 tangentDirection = normalize(i.tangDir);
				float atten;
				
				if(_WorldSpaceLightPos0.w == 0.0){
			   		atten = 1.0;
			   		lightDir = normalize(_WorldSpaceLightPos0.xyz);
			   	}
			   	else{
			 		float3 fragmentToLightSource = _WorldSpaceLightPos0.xyz - i.posWorld.xyz;
			   		float distance = length(fragmentToLightSource);
			   		atten = 1/distance;
			   		lightDir = normalize(fragmentToLightSource);
			   	}
				
				float4 tex = tex2D(_MainTex,i.tex.xy * _MainTex_ST.xy + _MainTex_ST.zw);
				float3 fragmentColor = tex.xyz*_Color.rgb*_UnlitColor.rgb;
				
				if(atten * saturate(dot(lightDir,normalDirection)) >= _DiffuseThreshold){
					fragmentColor = tex.xyz*_LightColor0.rgb*_Color.rgb;
				}
				
				return float4(fragmentColor,1.0);
			}
					
			ENDCG
		}
		
		Pass{
			Tags{"LightMode" = "ForwardAdd"}
			Blend One One
			Lighting on
			CGPROGRAM
			#pragma vertex vert
			#pragma fragment frag
			
			uniform float4 _Color;
			uniform float4 _UnlitColor;
			uniform float4 _LightColor0;
			uniform float4 _OutlineColor;
			uniform float4 _MainTex_ST;
			uniform float4 _LitColor;
			uniform float _DiffuseThreshold;
			uniform float _OutlineThickness;
			uniform sampler2D _MainTex;
			
			struct vertexInput{
				float4 pos : POSITION;
				float3 norm : NORMAL;
				float3 tang : TANGENT;
				float4 texCo : TEXCOORD0;
			};
			
			struct vertexOutput{
				float4 posOut : SV_POSITION;
				float4 posWorld : TEXCOORD0;
				float3 normalDir : TEXCOORD1;
				float3 tangDir : TEXCOORD2;
				float4 tex : TEXCOORD3;
			};
			
			vertexOutput vert(vertexInput i){
				vertexOutput o;
				o.posOut = mul(UNITY_MATRIX_MVP,i.pos);
				o.posWorld = mul(i.pos,_Object2World);
				o.normalDir = normalize(mul(float4(i.norm,1.0),_World2Object).xyz);
				o.tangDir = normalize(mul(float4(i.tang,1.0),_World2Object).xyz);
				o.tex = i.texCo;
				return o;
			}
			
			float4 frag(vertexOutput i):COLOR{
				float3 normalDirection = normalize(i.normalDir);
				float3 viewDirection = normalize(_WorldSpaceCameraPos - i.posWorld.xyz);
				float3 lightDir;
				float3 tangentDirection = normalize(i.tangDir);
				float atten;
				
				if(_WorldSpaceLightPos0.w == 0.0){
			   		atten = 1.0;
			   		lightDir = normalize(_WorldSpaceLightPos0.xyz);
			   	}
			   	else{
			 		float3 fragmentToLightSource = _WorldSpaceLightPos0.xyz - i.posWorld.xyz;
			   		float distance = length(fragmentToLightSource);
			   		atten = 1/distance;
			   		lightDir = normalize(fragmentToLightSource);
			   	}
				
				float4 tex = tex2D(_MainTex,i.tex.xy * _MainTex_ST.xy + _MainTex_ST.zw);
				float3 fragmentColor = _Color.rgb*_UnlitColor.rgb;
				
				if(atten * saturate(dot(lightDir,normalDirection)) >= _DiffuseThreshold){
					fragmentColor = _LightColor0.rgb*_Color.rgb;
				}
				
				return float4(fragmentColor,1.0);
			}
					
			ENDCG
		}
		
		Pass{
			Cull Front
			Lighting off
			CGPROGRAM
			uniform float4 _OutlineColor;
			uniform float _OutlineThickness;
			
			#pragma vertex vert
			#pragma fragment frag
			
			struct vIn{
				float4 posI : POSITION;
				float3 normI : NORMAL;	
			};
			
			struct vOut{
				float4 posO : SV_POSITION;
			};
			
			vOut vert(vIn i){
				vOut o;
				float3 norm = normalize(mul(float4(i.normI,1.0),_World2Object).xyz);
				float3 pos = i.posI.xyz;
				pos *= (1+_OutlineThickness);
				o.posO = mul(UNITY_MATRIX_MVP,i.posI + float4(norm.xy,0.0,0.0) * _OutlineThickness);
				return o;
			}
			
			float4 frag(vOut i):COLOR
			{
				return float4(0.0,0.0,0.0,1.0);
			}
			ENDCG
		}
	}
}