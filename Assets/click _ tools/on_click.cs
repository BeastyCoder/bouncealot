﻿using UnityEngine;
using System.Collections;

public class on_click : MonoBehaviour
{
	public GameObject Prefab;
	public int InstantiateType=0;
	private string[] InstantiateTypeNames = {"Mine", "Scene"};
	
	public bool showGui;
    private bool first = true;
	
    void Start()
    {
        //Debug.Log("clicked");
        
    }
    

	/*void OnClick()
	{
		Debug.Log ("clicked");
		if (PhotonNetwork.connectionStateDetailed != PeerState.Joined)
		{
			// only use PhotonNetwork.Instantiate while in a room.
			return;
		}
		
		switch (InstantiateType)
		{
		case 0:
			PhotonNetwork.Instantiate(Prefab.name, InputToEvent.inputHitPos + new Vector3(0, 5f, 0), Quaternion.identity, 0);
			Debug.Log (InputToEvent.inputHitPos + new Vector3(0, 5f, 0)+"   0");
			break;
		case 1:
			PhotonNetwork.InstantiateSceneObject(Prefab.name, InputToEvent.inputHitPos + new Vector3(0, 5f, 0), Quaternion.identity, 0, null);
			Debug.Log (InputToEvent.inputHitPos + new Vector3(0, 5f, 0)+"    1");
			break;
		}
	}*/
	
	void OnGUI()
	{
		if (showGui)
		{
			GUILayout.BeginArea(new Rect(Screen.width - 180, 0, 180, 50));
			InstantiateType = GUILayout.Toolbar(InstantiateType, InstantiateTypeNames);
			GUILayout.EndArea();
		}
	}

    void Update()
    {
        if (first) 
        {
            if (PhotonNetwork.connectionStateDetailed != PeerState.Joined)
            {
                // only use PhotonNetwork.Instantiate while in a room.
                return;
            }
           
            /*switch (InstantiateType)
            {
                case 0:
                    PhotonNetwork.Instantiate(Prefab.name, new Vector3(0, 10f, 0), Quaternion.identity, 0);
                    Debug.Log(InputToEvent.inputHitPos + new Vector3(0, 5f, 0) + "   0");
                    
                    break;
                case 1:
                    PhotonNetwork.InstantiateSceneObject(Prefab.name, new Vector3(0, 10f, 0), Quaternion.identity, 0, null);
                    Debug.Log(new Vector3(0, 10f, 0) + "    1");
                    break;
            }*/
            first = false;
        }
        
    }
	
	
}
