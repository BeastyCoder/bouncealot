﻿using UnityEngine;
using System.Collections;

public class CameraBehave : MonoBehaviour
{
    public float upDistance;
    public float awayDistance;
    public float rightDistance;
    public float smooth;
    private Transform follow;
    private Vector3 target;
    // Use this for initialization
    void Start()
    {

        follow = GameObject.FindGameObjectWithTag("PlayerFollower").transform;

    }

    // Update is called once per frame
    void LateUpdate()
    {
        target = follow.position + Vector3.up * upDistance - follow.forward * awayDistance + Vector3.right *rightDistance;
        transform.position = Vector3.Lerp(transform.position, target, smooth);
        transform.LookAt(follow);
    }
}
