﻿using UnityEngine;
using System.Collections;

public class Global : MonoBehaviour
{
    public RectTransform bgTransform;
    private float touch = 0.0f;
    private float elapsedTime = 0.0f;
    GameObject player;
    public string playerTag;
    // Use this for initialization
    void Start()
    {
       
        Screen.sleepTimeout = SleepTimeout.NeverSleep;
    }

    void OnGUI()
    {
        /*GUILayout.BeginArea(new Rect(Screen.width * 0.8f, Screen.height*0.8f, Screen.height * 0.7f, Screen.height * 0.2f));
        GUILayout.BeginHorizontal();
        GUILayout.Label("Touch : " + touch);
        GUILayout.EndHorizontal();
        GUILayout.BeginHorizontal();
        GUILayout.Label("Elapsed Time : " + elapsedTime);
        GUILayout.EndHorizontal();
        GUILayout.EndArea();*/
    }

    // Update is called once per frame
    void Update()
    {
        if (Input.GetKeyDown(KeyCode.Escape))
        {
            PhotonNetwork.Disconnect();
			Destroy(GameObject.Find("Join"));
			
            Application.LoadLevel("splash");
        }
		if(Input.GetKeyDown (KeyCode.F) || Input.GetKeyDown (KeyCode.KeypadEnter))
		{
			GetComponent<AbilityRunner>().UseAbility(1);
		}
        if(player == null)
        {
            try
            {
                foreach (GameObject pl in GameObject.FindGameObjectsWithTag("Player"))
                {
                    if (pl.GetComponent<PhotonView>().isMine)
                    {
                        player = pl;
                    }
                }
            }
            catch
            {
                player = null;
            }
        }

        else
        {
            touch = player.GetComponent<BallMultiMove>().touch;
            elapsedTime = player.GetComponent<BallMultiMove>().elapsedTime;
            //Debug.Log(touch);
        }
    }
}
