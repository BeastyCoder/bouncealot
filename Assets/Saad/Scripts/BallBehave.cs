﻿using UnityEngine;
using System.Collections;

public class BallBehave : MonoBehaviour
{

    Rigidbody rb;
    public float magnitude = 1f;
    public float maxSpeed = 25f;
    public float bumpForce = 0.2f;
    private float bounceStrength = 0.4f;
    private float touch = 0f;
    private float elapsedTime = 0f;
    public float timeWindow = 1f;
    public bool debug = false;
    public Texture2D tex = null;
    public bool enabled = true;
    Vector3 up;
    // Use this for initialization
    void Start()
    {
        rb = GetComponent<Rigidbody>();
        up = new Vector3(0, 1, 0);
    }



    void OnMouseDown()
    {
        touch = 1 - touch;

        if (touch == 1) elapsedTime = 0f;
    }


    void OnCollisionEnter(Collision col)
    {
        if (!enabled) return;
        if (col.gameObject.tag == "Wall")
        {

            rb.AddForce(Vector3.Reflect(transform.position, col.contacts[0].normal) * bounceStrength, ForceMode.Impulse);
            Vector3 n = Vector3.Reflect(transform.position, col.contacts[0].normal);
            magnitude -= Vector3.Magnitude(n) * bumpForce * 1.1f;
            Debug.Log(magnitude);

        }
        if (col.gameObject.name == "Player1")
        {
            col.gameObject.rigidbody.AddForce(Vector3.Reflect(transform.position, col.contacts[0].normal) * bumpForce * (magnitude * 0.5f), ForceMode.Impulse);
            col.gameObject.rigidbody.AddForce(up * 5f * touch, ForceMode.Impulse);

            Debug.Log("Entered");
        }
    }

    void Update()
    {
        if (!enabled) return;
        if (debug)
        {
            Vector3 playerPos = GameObject.FindGameObjectWithTag("Player").transform.position;
            Vector3 player1Pos = GameObject.FindGameObjectWithTag("Player1").transform.position;

            Debug.DrawLine(playerPos, up * 5f, Color.blue);
            Debug.DrawLine(player1Pos, new Vector3(0, player1Pos.y, 0) * 5f, Color.magenta);
        }
    }

    // Update is called once per frame
    void FixedUpdate()
    {
        if (!enabled) return;
        /*if(Input.GetTouch(0).phase == TouchPhase.Began){
            touch = 1 - touch;
            if(touch == 1) elapsedTime = 0f;
        }*/

        if (Input.GetMouseButtonDown(0))
        {
            touch = 1 - touch;

            if (touch == 1) elapsedTime = 0f;
        }
        float moveHorizontal = Input.GetAxis("Horizontal");          //acceleration.x;GetAxis("Horizontal");
        float moveVertical = Input.GetAxis("Vertical");             //acceleration.y;GetAxis("Vertical");
        if (moveHorizontal != 0 || moveVertical != 0)
        {

            rb.AddForce(new Vector3(moveHorizontal * magnitude, 0, moveVertical * magnitude) * Time.deltaTime * 50f);

            if (magnitude < maxSpeed)
                magnitude += Vector3.Magnitude(new Vector3(moveHorizontal, 0, moveVertical) * 0.1f * Time.deltaTime * 50f);
        }
        else
        {
            if (magnitude > 5f)
                magnitude -= 5f;
        }
        if (touch > 0)
        {
            if (elapsedTime < timeWindow)
            {
                elapsedTime += Time.deltaTime;
                touch = 1 - elapsedTime * 0.45f;
            }
            else
            {
                touch = 0f;
                elapsedTime = 0f;
            }
        }

        if (magnitude > 25) magnitude = 25f;
        if (magnitude < 0) magnitude = 0;

    }


}
