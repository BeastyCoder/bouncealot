﻿using UnityEngine;
using System.Collections;

public class ToogleInput : MonoBehaviour {
    BallBehave accel;
    ball_movement joys;
	// Use this for initialization
	void Start () {
        accel = GetComponent<BallBehave>();
        joys = GetComponent<ball_movement>();
	}
	
   /* void OnGUI()
    {
        GUILayout.BeginArea(new Rect(20, 100, Screen.width / 2, 100));
        GUILayout.BeginHorizontal();
        GUILayout.Label("accel ; " + accel.enabled);
        GUILayout.Label("joys : " + joys.enabled);
        GUILayout.EndHorizontal();
        GUILayout.EndArea();
    }*/



	// Update is called once per frame
	void Update () {
        if (Input.GetKey(KeyCode.F))
        {
            if (accel.enabled == true) accel.enabled = false;
            else accel.enabled = true;
            if (joys.enabled == true) joys.enabled = false;
            else joys.enabled = true;
        }
	}
}
