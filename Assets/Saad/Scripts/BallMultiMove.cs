﻿using UnityEngine;
using System.Collections;

public class BallMultiMove : MonoBehaviour
{
    //Variables from first
    Rigidbody rb;
    public float magnitude = 1f;
    public float maxSpeed = 25f;
    public float bumpForce = 0.2f;
    private float bounceStrength = 0.4f;
    public float touch = 0f;
    public float elapsedTime = 0f;
    public float timeWindow = 1f;
    public float sensitivity = 0f;
    private float senseFact = 1f;
    public bool debug = false;
    public bool enabled = true;
    private bool fingerHold = false;
    public int id;
    Vector3 forceDir = Vector3.zero;
    int targetId;
    Vector3 up;
    Collision c;
    //Variables from second
    public Joystick joyStickLeft = null;
    public Joystick joyStickRight = null;
    public float rotationSpeed = 0.5f;

    void Awake()
    {
        

        if (PlayerPrefs.HasKey("sensitivity"))
            sensitivity = PlayerPrefs.GetFloat("sensitivity")*0.0001f;
        if (PlayerPrefs.HasKey("joystick"))
            enabled = PlayerPrefs.GetInt("joystick") == 1 ? false : true;
    }

    //Start
    void Start()
    {
        if (!enabled)
        {
            if (joyStickLeft == null)
            {
                GameObject objjoyStickLeft = GameObject.FindGameObjectWithTag("leftjoystick") as GameObject;
                joyStickLeft = objjoyStickLeft.GetComponent<Joystick>();
            }

            if (joyStickRight == null)
            {
                GameObject objjoyStickRight = GameObject.FindGameObjectWithTag("rightjoystick") as GameObject;
                joyStickRight = objjoyStickRight.GetComponent<Joystick>();
            }
        }
        else
        {
            GameObject objjoyStickRight = GameObject.FindGameObjectWithTag("rightjoystick") as GameObject;
            objjoyStickRight.GetComponent<GUITexture>().enabled = false;
            joyStickRight = objjoyStickRight.GetComponent<Joystick>();
            joyStickRight.enabled = false;

        }
        id = PhotonNetwork.player.ID;
        rb = GetComponent<Rigidbody>();
        up = new Vector3(0, 1, 0);
        if(PlayerPrefs.HasKey ("sensitivity"))
        	sensitivity = PlayerPrefs.GetFloat ("sensitivity");
        else{
        	PlayerPrefs.SetFloat ("sensitivity",1.0f);
        	sensitivity = 1.0f;
        }
        sensitivity *= 0.001f;
    }



    //OnCollision
    void OnCollisionEnter(Collision col)
    {
        
        if (col.gameObject.tag == "Player" && GetComponent<PhotonView>().isMine)
        {
            Vector3 dir = -col.rigidbody.velocity.normalized;
            rb.rigidbody.AddForce(dir * bumpForce * (magnitude * 0.5f), ForceMode.Impulse);
            
            forceDir = (col.gameObject.transform.position - transform.position).normalized + rigidbody.velocity.normalized;
            targetId = col.gameObject.GetComponent<PhotonView>().ownerId;
            //Debug.Log(col.gameObject.GetComponent<PhotonView>().ownerId);
            Debug.Log(rigidbody.velocity.magnitude);
            if (GetComponent<PhotonView>().isMine)
                GetComponent<RPCTest>().AddForce(forceDir, rigidbody.velocity.magnitude, bumpForce, targetId);
            forceDir = Vector3.zero;
            //Debug.Log("Entered");
        }

        //Debug.Log(col.gameObject.name);
    }

    void FixedUpdate()
    {
        float moveHorizontal;
        float moveVertical;

        if (enabled) // Accelerometer
        {
            moveHorizontal = Input.acceleration.x;       //acceleration.x;GetAxis("Horizontal");
            moveVertical = Input.acceleration.y;             //acceleration.y;GetAxis("Vertical");
			senseFact += sensitivity;
            //Debug.Log(moveHorizontal + " " + moveVertical);
            
        }
        else // Joystick
        {
            moveHorizontal = joyStickRight.position.x;
            moveVertical = joyStickRight.position.y;
            senseFact = 1;
        }

        if (Input.GetMouseButtonDown(0) && enabled)
        {

            touch = 1 - touch;

            if (touch == 1) elapsedTime = 0f;
        }

        else
        {

            int nbTouches = Input.touchCount;

            if (nbTouches > 0)
            {
                for (int i = 0; i < nbTouches; i++)
                {
                    Touch t = Input.GetTouch(i);

                    if (t.position.x > Screen.width * 0.5 && nbTouches > 1)
                    {
                        touch = 1 - touch;
                        if (touch == 1) elapsedTime = 0f;
                        break;
                    }
                }
            }
        }


        if (Mathf.Abs(moveHorizontal) > 0.1f || Mathf.Abs(moveVertical) > 0.1f)
        {

            rb.AddForce(new Vector3(moveHorizontal * magnitude, 0, moveVertical * magnitude) * Time.deltaTime * 50f * senseFact);
            
            if (magnitude < maxSpeed)
                magnitude += Vector3.Magnitude(new Vector3(moveHorizontal, 0, moveVertical) * 0.1f * Time.deltaTime * 50f * senseFact);
        }
        else
        {
            if (magnitude > 1f)
                magnitude -= (10f * Time.deltaTime);
        }
        if (touch > 0)
        {
            if (elapsedTime < timeWindow)
            {
                elapsedTime += Time.deltaTime;
                touch = 1 - elapsedTime * 0.45f;
            }
            else
            {
                touch = 0f;
                elapsedTime = 0f;
            }
        }

        if (magnitude > 25) magnitude = 25f;
        if (magnitude < 1) magnitude = 1;

       
    }
   

}
