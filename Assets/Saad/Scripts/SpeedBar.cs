﻿using UnityEngine;
using System.Collections;
using UnityEngine.UI;
public class SpeedBar : MonoBehaviour {

    public RectTransform speedTransform;
    private float speedY;
    private float minX;
    private float maxX;
    private float curSpeed;
    public float maxSpeed;
    public Image visualSpeed;
    private GameObject player;
    public string plTag;
	// Use this for initialization
	void Start () {
        speedY = speedTransform.position.y;
        maxX = speedTransform.position.x;
        minX = maxX - speedTransform.rect.width;
        //Debug.Log(maxX + " " + minX);
        
        //curSpeed = (float)GetComponent<BallMultiMove>().magnitude;
	}
	
	// Update is called once per frame
	void Update () {
        if (player == null)
        {
            try
            {
                player = GameObject.FindGameObjectWithTag(plTag);

            }
            catch
            {
                player = null;
            }
        }
        else
        {
            curSpeed = (float)player.GetComponent<BallMultiMove>().magnitude;
            float curX = MapValues(curSpeed, 0, maxSpeed, minX, maxX);
            speedTransform.position = new Vector3(curX, speedY, 0);

            if (curSpeed > maxSpeed / 2)
            {
                visualSpeed.color = new Color32(255, (byte)MapValues(curSpeed, maxSpeed / 2, maxSpeed, 255, 0), 0, 255);
            }
            else if (curSpeed < maxSpeed)
            {
                visualSpeed.color = new Color32((byte)MapValues(curSpeed, maxSpeed / 2, maxSpeed, 0, 255), 255, 0, 255);
            }
        }
	}

    void OnGUI()
    {
        GUILayout.BeginArea(new Rect(20, 60, Screen.width / 2, 100));
        GUILayout.BeginHorizontal();
        GUILayout.Label("curSpeed : " + curSpeed);
        GUILayout.EndHorizontal();
        GUILayout.EndArea();
    }

    private float MapValues(float x, float inMin, float inMax, float outMin, float outMax)
    {
        return (x - inMin) * (outMax - outMin) / (inMax - inMin) + outMin;
    }
}
