﻿using UnityEngine;
using System.Collections;


// //            MANUAL -- IMPORTANTe
//for making the script work
// import standard assets (mobile)
// now drag dual joysticks from it to heirarchy
// now tag right and left joystick to "rightjoystick" & "leftjoystick"
// now attach the script to the ball

//

public class ball_movement : MonoBehaviour
{

    public Joystick joyStickLeft = null;
    public Joystick joyStickRight = null;
    public bool enabled = false;
    public float rotationSpeed = 0.5f;

    // edited
    Rigidbody rb;
    public float magnitude = 1f;
    public float maxSpeed = 25f;
    public float bumpForce = 0.2f;
    private float bounceStrength = 0.4f;
    private float touch = 0f;
    private float elapsedTime = 0f;
    public float timeWindow = 1f;
    public bool debug = false;
    public Texture2D tex = null;
    Vector3 up;
    void Start()
    {

        // this is to set the joysticks
        if (joyStickLeft == null)
        {
            GameObject objjoyStickLeft = GameObject.FindGameObjectWithTag("leftjoystick") as GameObject;
            joyStickLeft = objjoyStickLeft.GetComponent<Joystick>(); ;
        }

        if (joyStickRight == null)
        {
            GameObject objjoyStickRight = GameObject.FindGameObjectWithTag("rightjoystick") as GameObject;
            joyStickRight = objjoyStickRight.GetComponent<Joystick>(); ;
        }
        // edited
        rb = GetComponent<Rigidbody>();
        up = new Vector3(0, 1, 0);
    }


    // / on collission
    void OnCollisionEnter(Collision col)
    {
        if (!enabled) return;
        if (col.gameObject.tag == "Wall")
        {

            rb.AddForce(Vector3.Reflect(transform.position, col.contacts[0].normal) * bounceStrength, ForceMode.Impulse);
            Vector3 n = Vector3.Reflect(transform.position, col.contacts[0].normal);
            magnitude -= Vector3.Magnitude(n) * bumpForce * 1.1f;
            Debug.Log(magnitude);

        }
        if (col.gameObject.name == "Player1")
        {
            col.gameObject.rigidbody.AddForce(Vector3.Reflect(transform.position, col.contacts[0].normal) * bumpForce * (magnitude * 0.5f), ForceMode.Impulse);
            col.gameObject.rigidbody.AddForce(up * 5f * touch, ForceMode.Impulse);

            Debug.Log("Entered");
        }
    }

    void FixedUpdate2()
    {
        float moveHorizontal = Input.GetAxis("Horizontal");           //acceleration.x;
        float moveVertical = Input.GetAxis("Vertical");             //acceleration.y;
        if (moveHorizontal != 0 || moveVertical != 0)
        {

            rb.AddForce(new Vector3(moveHorizontal * magnitude, 0, moveVertical * magnitude));

            if (magnitude < maxSpeed)
                magnitude += Vector3.Magnitude(new Vector3(moveHorizontal, 0, moveVertical) * 0.1f);
        }
        else
        {
            if (magnitude > 5f)
                magnitude -= 5f;
        }

    }

    // Update is called once per frame
    void FixedUpdate()
    {
        if (!enabled) return;
        float moveHorizontal = joyStickLeft.position.x;          //acceleration.x;
        float moveVertical = joyStickLeft.position.y;

        if (moveHorizontal != 0 || moveVertical != 0)
        {

            rb.AddForce(new Vector3(moveHorizontal * magnitude, 0, moveVertical * magnitude));

            if (magnitude < maxSpeed)
                magnitude += Vector3.Magnitude(new Vector3(moveHorizontal, 0, moveVertical) * 0.1f);
        }
        else
        {
            if (magnitude > 5f)
                magnitude -= 5f;
        }
        /*	

            Vector3 position = this.transform.position;
			
            float xMovement = 0;
            float yMovement = 0;
            // this is to move the cube
            if(joyStickLeft.position.x<0) { 		
                xMovement = xMovement - .1f; 	
            } 		
            if(joyStickLeft.position.x>0) {
                xMovement = xMovement + .1f;
            }
			
            if(joyStickLeft.position.y<0) { 	
            yMovement = yMovement - .1f; 		
            } 		
            if(joyStickLeft.position.y>0) {
                yMovement = yMovement + .1f;
				
            }
			
            position.x = position.x+ xMovement;
            position.z = position.z + yMovement;
            this.transform.position = position;
			
            // to rotate the cube
            float rotatePos = joyStickInput(joyStickRight);
            transform.Rotate(0, rotatePos * rotationSpeed, 0); */

    }

    float joyStickInput(Joystick jstick)
    {
        Vector2 absJoyPos = new Vector2(Mathf.Abs(jstick.position.x),
                                        Mathf.Abs(jstick.position.y));
        int xDirection = (jstick.position.x > 0) ? 1 : -1;
        int yDirection = (jstick.position.y > 0) ? 1 : -1;
        return ((absJoyPos.x > absJoyPos.y) ? absJoyPos.x * xDirection : absJoyPos.y * yDirection);
    }

}