﻿using UnityEngine;
using System.Collections;

public class PlayerFollower : MonoBehaviour {
	private Transform follow;
	private Vector3 target;
    public string followTag;
	// Use this for initialization
	void Start () {

	}
	
	// Update is called once per frame
	void Update () {
        if (follow == null)
        {
            try
            {
                foreach(GameObject o in GameObject.FindGameObjectsWithTag(followTag))
                {
                    if(o.GetComponent<PhotonView>().isMine)
                    {
                        follow = o.gameObject.transform;
                    }
                }

            }
            catch
            {
                follow = null;
            }
        }
        else
        {
            //Debug.Log("Following");
            target.x = follow.transform.position.x;
            target.y = follow.transform.position.y;
            target.z = follow.transform.position.z;
            transform.position = target;
        }
	}
}
